# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 23:13:14 2016

@author: colormotor
"""

#!/usr/bin/env python
import urllib
import urllib2
import json
import os
import sys

# Modify this appropriately
download_path = '~/data/skeletor/downloaded/'

list_images_url = 'http://phylopic.org/api/a/image/list/%d/%d?options=string'
# find name id matches for a text string
name_search_url = 'http://phylopic.org/api/a/name/search?text=%s&options=icon'
# find name id matches for a text string
taxa_search_url = 'http://phylopic.org/api/a/name/%s/taxonomy?supertaxa=all'
# find images associated with a name id
image_search_url = 'http://phylopic.org/api/a/name/%s/images?subtaxa=true'
# image from image id
image_urls = {
    'thumb.png':  'http://phylopic.org/assets/images/submissions/%s.thumb.png',
    'svg':        'http://phylopic.org/assets/images/submissions/%s.svg',
    'png':        'http://phylopic.org/assets/images/submissions/%s.256.png'
    }

image_name_url = 'http://phylopic.org/api/a/image/%s?options=taxa+string'

def retrieve_image_taxa(uid):
    try:
        response = urllib2.urlopen(image_name_url % uid)
        data = json.loads(response.read())
        response.close()
    except urllib2.HTTPError:
        print 'HTTPErr, image not found'
        return uid
        
    if not data['success']:
        print 'Could not retrieve name, using uid'
        return uid
        
    return data['result']['taxa'][0]['canonicalName']['string']
    
    
    
def list_image_uids(start, num):
    url = list_images_url % (start, num)
    
    response = urllib2.urlopen(url)
    data = json.loads(response.read())
    response.close()
    
    assert data['success']
    
    
    res = []
    for entry in data['result']:
        print entry 
        res.append(entry['uid'])
    return res
    


#vv = retrieve_image_taxa('9fae30cd-fb59-4a81-a39c-e1826a35f612')

def download_images(uids, img_format='png'):
    img_url = image_urls[img_format]
    
    for uid in uids:
        url = img_url % uid
        #fname = '_'.join( name.split() ) + '.' + img_format
        name = retrieve_image_taxa(uid)
        name = name.split('/')[-1]
        name = '_'.join( name.split() )
        
        # 
        fname = download_path + name + '.' + img_format
        
        print 'downloading: ' + url
        try:
            request = urllib2.urlopen(url)
            with open(fname, 'wb') as image_file:
                while True:
                    data = request.read(1024)
                    if not data:                         
                        print 'Downloaded ' + fname
                        break
                    image_file.write(data)
                
        except urllib2.HTTPError:
            print 'Http error'
            
#def pic_search(taxon):
#    url = name_search_url % urllib.quote_plus(taxon)
#
#    response = urllib2.urlopen(url)
#    data = json.loads(response.read())
#    response.close()
#    
#    assert data['success']
#    
#    for name in data['result']:
#        for result in (name.get('icon',None) 
#            if result: result = result.get('uid', None)
#            if result: yield result

if __name__ == '__main__':
    
    start = 0
    n = 20
    if len(sys.argv) == 3:
        start = int(sys.argv[1])
        n = int(sys.argv[2])

    L = list_image_uids(start, n)
    download_images(L)
