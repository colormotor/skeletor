# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 16:27:04 2016

@author: colormotor
"""

import os 
import skeletor as skel

skel.config.debug_draw = False
skel.config.verbose = False

def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

data_dir = os.path.expanduser('~/data/skeletor/')

create_dir(data_dir + 'lo_graphs')
create_dir(data_dir + 'med_graphs')
create_dir(data_dir + 'hi_graphs')
create_dir(data_dir + 'lo_strokes')
create_dir(data_dir + 'med_strokes')
create_dir(data_dir + 'hi_strokes')
create_dir(data_dir + 'lo_junctures')
create_dir(data_dir + 'med_junctures')
create_dir(data_dir + 'hi_junctures')


# Load images
imgs = []



for file in os.listdir(data_dir + 'downloaded'):
    if file.endswith(".png"):
        imgs.append(data_dir+'downloaded/'+file) #print(file)
            
def filename(path):
    p = path.split('/')[-1]
    return p.split('.')[0]
    
def parse(img_path, simp, dest_dir):
    G, S, Sj = parse_image(img_path, simplify_eps=simp)
    fname = filename(img)
    save_strokes(data_dir + dest_dir + '_strokes/' + fname + '.txt', S)
    save_graph(data_dir + dest_dir + '_graphs/' + fname + '.txt', G)
    save_stroke_junctures(data_dir + dest_dir + '_junctures/' + fname + '.txt', Sj)
    
for i in range(10, len(imgs)):
    img = imgs[i]
    try:
        parse(img, 1, 'hi')
        parse(img, 3, 'med')
        parse(img, 7, 'lo')
    except:
        print "Failed : " + img
    
            
        

    