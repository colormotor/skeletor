###########################################################################
### Dirty and work in progress skeletonization and stroke extraction code
### Medial Axis code partially adapted from https://github.com/cdglabs/tableVision
### The graph extraction and simplification code is based on the following papers:
### J. Feldman, M. Singh, 2006, Bayesian estimation of the shape skeleton
### W. Shen et al. 2013, Skeleton pruning as trade-off between skeleton simplicity and reconstruction error
### X. Wang et ak. 2013, Triangular mesh based stroke segmentation for chinese calligraphy
### The system computes a naive ordering of strokes using a genetic algorithm.
### 
### © Daniel Berio, 2016
### d.berio@gold.ac.uk
### http://www.enist.org
###
###########################################################################

import numpy as np
from scipy import ndimage as ndi
from skimage.morphology import medial_axis
from skimage.io import imread
from skimage.filter import threshold_otsu, threshold_yen

import itertools
import matplotlib.pyplot as plt
import networkx as nx
from numpy.linalg import norm, det
import math

from scipy.interpolate import interp1d
import cv2



#################################################################
### Configuration
class Config:
    def __init__(self):
        self.debug_draw = True
        self.verbose = False

config = Config()

#################################################################
### Utils

def debug_print(s):
    if config.verbose:
        s = str(s)
        print s

def vec_to_string(X, delimiter):
    res = ''
    for x in X:
        res += str(x) + delimiter

    res = res[:-1]
    res += '\n'
    return res

def np_to_string(a, delimiter=','):
    shape = a.shape
    if len(shape) == 1:
        return vec_to_string(a, delimiter)

    res = ''
    for row in a:
        res += vec_to_string(row, delimiter)

    return res
    
def string_to_np(s, delimiter=','):
    S = s.split()
    if len(S) == 1:
        return np.fromstring(s, sep=delimiter)
    V = np.array([ np.fromstring(s, sep=delimiter) for s in S ])
    return V

def arrays_to_string( A, delimiter=',', array_delimiter=';'):
    s = ""
    for a in A:
        s += np_to_string(a, delimiter)
        s += array_delimiter + "\n"
    s = s[:-2]
    s += '\n'
    return s

def arrays_from_string( s, delimiter=',', array_delimiter=';'):
    As = s.split(array_delimiter)
    return [ string_to_np(s) for s in As ]

def save_arrays(path, A, delimiter=',', array_delimiter=';'):
    s = arrays_to_string(A, delimiter, array_delimiter)
    f = open(path,'w')
    f.write(s)
    f.close()
    
#################################################################
### Plotting Utils

v2 = lambda x,y: np.array([x,y])

def plt_circle(p, radius, color='r', alpha=1.0, fill=False):
    plt.gca().add_patch(plt.Circle(p, radius=radius, color=color, alpha=alpha, fill=fill))

def plt_line(a, b, color='r'):
    plt.plot([a[0], b[0]], [a[1], b[1]], color)

def plt_arrow(a, b, size, color='r'):
    plt.gca().arrow(a[0], a[1], b[0]-a[0], b[1]-a[1], head_width=size*0.7, head_length=size, fc=color, ec=color)

def plt_skeleton_branch(P, node_radius=3., color='r', alpha=1.0):
    plt.plot(P[0,:], P[1,:], color)
    plt_circle(P[:,0], node_radius, color=color, alpha=alpha)
    plt_circle(P[:,-1], node_radius, color=color, alpha=alpha)
    
#################################################################
### Geom Utils
    
def distance(a, b):
    return norm(b-a)

def distance_sq(a, b):
    return np.dot(b-a, b-a)

def point_line_distance(p, a, b):
    if np.array_equal(a,b):
        return distance(p, a)
    else:
        return abs(det(np.array([b-a, a-p]))) /  norm(b-a)


def contours_to_np(ctrs):
    return [ np.reshape(C, (C.shape[0],C.shape[2])).T.astype(float) for C in ctrs]


def normalize_signal(X):
    return (1.0/np.max(X)-np.min(X)) * (X - min(X))
    

def columnwise_length(D):
    return np.sqrt( D[0,:]**2 + D[1,:]**2 )
    
def angle_between(a, b):
    """ returns angle between two vectors a and b"""
    theta = math.atan2( det([a, b]), np.dot(a,b) )
    return theta

def radians( x ):
    return np.pi/180*x

def degrees( x ):
    return x * (180.0/np.pi)
    

def normc(X):
    """ Normalizes columns of X"""
    l = np.sqrt( X[0,:]**2 + X[1,:]**2 )
    Y = np.array(X)
    Y[0,:] = Y[0,:] / l
    Y[1,:] = Y[1,:] / l
    return Y
    
def curvature(P, closed=0):
    
    if closed:
        P = np.c_[P[:,-1], P, P[:,0]]
        
    D = np.diff(P, axis=1)
    l = np.sqrt( D[0,:]**2 + D[1,:]**2 )
    D[0,:] /= l
    D[1,:] /= l
        
    n = D.shape(1) #size(D,2);
    
    theta = [ angle_between(a, b) for a, b in zip(D.T, D.T[1:]) ]
    
    K = 2*np.sin(theta/2) / np.sqrt( l[:-1] *l[1:] )
    
    if not closed:
        K = np.concatenate([[K[0]], K, [K[-1]]])
        
    return K

def normals_2d(P, closed=0):
    if closed:
        P = np.c_[P[:,-1], P, P[:,0]]
        
    D = np.diff(P, axis=1)
    
    T = D[:,:-1] + D[:,1:]
    
    if not closed:
        T = np.c_[T[:,0], T, T[:,-1]]
    
    N = np.dot([[0,-1],[1, 0]], T)
    return normc(N)

def turning_angles(P, closed=0):
    N = normals2d(P, closed);
    
    if closed:
        N = np.c_[N, N[:,0]]
    else:
        N = np.c_[N, N[:,-1]]
    
    n = N.shape(1)
    
    A = [ angle_between(a, b) for a, b in zip(N.T, N.T[1:]) ]
    
    return A

    
def surprisal(P, b, closed=0):
    ''' Surprisal of turning angle a-la 
        Feldman and Singh 2006, Information along contours and object boundaries. '''
    A = turning_angles(P, closed)
    S = -np.log( np.exp(b*np.cos(A) ) ) #/( 2*pi*besseli(0,b) ) );
    return S
    
def normal_segments(P, N):
    ''' Returns a series of segments between each point
        in P and pointing in directions N'''
    Nx = np.vstack([P[0,:], P[0,:]+N[0,:]])
    Ny = np.vstack([P[1,:], P[1,:]+N[1,:]])
    return Nx, Ny

def chord_lengths( P, closed=0 ):
    ''' Chord lengths for each segment of a contour '''
    if closed:
        P = np.c_[P, P[:,0]]
    D = np.diff(P, axis=1)
    L = np.sqrt( D[0,:]**2 + D[1,:]**2 )
    return L

def cum_chord_lengths( P, closed=0 ):
    ''' Cumulative chord lengths '''
    L = chord_lengths(P, closed)
    return np.cumsum(np.concatenate([[0.0],L]))

def chord_length( P, closed=0 ):
    ''' Chord length of a contour '''
    L = chord_lengths(P, closed)
    return np.sum(L)

def uniform_sample( P, dist, closed=0, data=None ):
    ''' Uniformly samples a contour at a step dist'''
    l = chord_length(P, closed)    
    x = cum_chord_lengths( P, closed ) / l
    
    x[-1] = 1.0 # corrects for precision errors 
    
    n = int(l / dist)
    if closed:
        P = np.c_[P, P[:,0]]
        
    f = interp1d(x,P,kind='linear')
    x_new = np.linspace(0, 1, n)
    Y = f(x_new)
    
    if data!=None:
        f = interp1d(x,data,kind='linear')
        data = f(x_new)
        return Y, data
        
    return Y

def flip(p=0.5):
    ''' Flip a coin with probability p'''
    return 1 if np.random.uniform() < 5 else 0
    
def vflip(n, p=0.5):
    ''' Flip a coin n times with probability p'''
    return (np.random.uniform(size=n) < p).astype(int)
    
#################################################################
### MA code adapted from https://github.com/cdglabs/tableVision
def update_attributes(G):
    dist = G.graph['dist']
    for x,y in G.nodes():
        G.node[(x,y)]['d'] = dist[y,x]
        G.node[(x,y)]['p'] = v2(x,y)
        
def compute_ma( img, junct_eps=2, simp_eps=0 ):
    #img = img > 0
    debug_print('computing medial axis')
    # Compute the medial axis (skeleton) and the distance transform
    skel, dist = medial_axis(img, return_distance=True)
    
    debug_print('generating graph')
    G = gen_graph(skel, dist)    
    G.graph['dist'] = dist
    
    update_attributes(G)
    #print G
    G = simplify_junctures(G, epsilon=junct_eps)
    
    update_attributes(G)

    return G

def skeleton_branches(G, extremities_only=False):
    ''' Returns the branches of the skeletong graph, 
        with extremities_only=True this will return only the branches 
        that end with a vertex of degree 1 '''
        
    visited = {n:0 for n in G.nodes()}
    branches = []
    
    def can_traverse(n):
        deg = G.degree(n)
        #print 'degree: ' + str(deg)
        if deg <= 2:
            #print '_visited: ' + str(visited[n])
            return not visited[n]
        else:
            #print 'visited: ' + str(visited[n])
            return visited[n] < deg
            
    def traverse_branch(branch, n, prev):
        visited[n] = visited[n] + 1 
        branch.append(n)
        
        # end condition
        if len(branch) > 1 and G.degree(n) != 2:
            return
        
        # traverse 
        for b in G.neighbors(n):
            if can_traverse(b) and b != prev:
                traverse_branch(branch, b, n)
                return
       
    end_nodes = find_termination_junctures(G) #[ n for n in G.nodes() if G.degree(n) == 1 ]
    fork_nodes = find_nodes_with_degree(G, lambda degree: degree > 2)
    
    for n in end_nodes:
        if not visited[n]:
            branch = []
            traverse_branch(branch, n, None)
            if len(branch) > 1:
                branches.append(branch)
            else:
                debug_print('Corrupt branch')
                debug_print(branch)
    
    if extremities_only:
        return branches
        
        
    # Visit fork nodes iteratively until all branches are found
    visit_forks = True
    
    while visit_forks:  
        for n in fork_nodes:
            if visited[n] < G.degree(n):
                branch = []
                traverse_branch(branch, n, None)
                if len(branch) > 1:
                    branches.append(branch)
        
                else:
                    print 'Corrupt branch'
                    print branch
                    
        visit_forks = False
        # Check if anything left
        for n in fork_nodes:
            if visited[n] < G.degree(n):
                visit_forks = True
    
    # There might still be cycles that are isolated 
    # So search for unvisited nodes
    
    if 0 in visited.values():
        unvisited = [] 
        for n, v in visited.items():
            if v==0:
                unvisited.append(n)
        
        for n in unvisited:
            if not visited[n]:
                branch = []
                traverse_branch(branch, n, None)
                if len(branch) > 1:
                    branches.append(branch)
                
    return branches


def extremity_branches(G):
    return skeleton_branches(G, extremities_only=True)
    
def branch_to_contour(branch):
    ''' Creates a contour from a branch'''
    P = np.zeros((2, len(branch)))
    for i in range(len(branch)):
        P[:,i] = v2(branch[i][0], branch[i][1]) #G.node[branch[i]]['p']
    return P
   
########################################################
########################################################
### These parts of the code are adapted from https://github.com/cdglabs/tableVision
    
def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ...
    via https://docs.trans-module imports.org/2/library/itertools.html
    """
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)


def adjacent_coords(x, y, w, h):
    def in_img(x,y):
        return x >= 0 and x < w and y >= 0 and y < h
    
    adj = [(-1,-1),(0,-1),(1,-1),
           (-1,0),(1,0),
           (-1, 1),(0,1),(1,1)]
    
    return [(x+ox, y+oy) for ox,oy in adj
            if in_img(x+ox, y+oy)]

def quadrance((x1,y1), (x2,y2)):
    """Returns distance-squared between two points."""
    dx = x2 - x1
    dy = y2 - y1
    return dx*dx + dy*dy


def distance_int(p1, p2):
    return math.sqrt(quadrance(p1, p2))

def point_line_distance_int(point, start, end):
    if (start == end):
        return distance_int(point, start)
    else:
        n = abs(
            (end[0] - start[0]) * (start[1] - point[1]) -
            (start[0] - point[0]) * (end[1] - start[1])
        )
        d = math.sqrt(
            (end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2
        )
        return n / d



def rdp( branch, eps ):
    """ Ramer Douglas Peucker (see Douglas1973) polyline simplification on a branch of a graph"""
    if eps==0.0:
        return branch
    
    dmax = 0.0
    index = 0
    for i in range(1, len(branch) - 1):
        d = point_line_distance_int(branch[i], branch[0], branch[-1])
        if d > dmax:
            index = i
            dmax = d
    if dmax >= eps:
        results = rdp(branch[:index+1], eps)[:-1] + \
            rdp(branch[index:], eps)
    else:
        results = [branch[0], branch[-1]]
    return results


def gen_graph(skel, dist):
    """Takes in a skeletonized image (with foreground as 255 and background as
    0) and produces a graph of appropriately connected pixel locations.
    """
    G = nx.Graph()
    (h, w) = skel.shape
    G.graph['img_size'] = (w,h)
    G.graph['dist'] = dist
    
    for y in xrange(h):
        for x in xrange(w):
            v = skel[y,x]
            d = dist[y,x]
            if v:
                G.add_node((x,y)) #,color=d)
                

    for y in xrange(h):
        for x in xrange(w):
            v = skel[y,x]
            if v:
                for xx,yy in adjacent_coords(x, y, w, h):
                    if skel[yy,xx]:
                        G.add_edge((x,y), (xx,yy))
    
    
    return G



def find_junctures(graph):
    return find_nodes_with_degree(graph, lambda degree: degree > 2)



def find_termination_junctures(graph):
    return find_nodes_with_degree(graph, lambda degree: degree == 1)



def find_nodes_with_degree(graph, filter_function):
    junctures = []
    for node in nx.nodes_iter(graph):
        degree = nx.degree(graph, node)
        if filter_function(degree):
            junctures.append(node)
    return junctures

def remove_branch(G, branch):
    # hacky, expecting to start from extremity....
    for i in branch[:-1]:
        for n in G.neighbors(i):
            G.remove_edge(i,n)

def find_clumps(graph, epsilon):
    """Returns a list of "clumps". Each clump is a set of junctures which are
    within epsilon of each other."""
    max_quadrance = epsilon * epsilon
    junctures = find_junctures(graph)
    clump_graph = nx.Graph()
    for juncture in junctures:
        clump_graph.add_node(juncture)
    for (i, j) in itertools.combinations(junctures, 2):
        if quadrance(i, j) < max_quadrance:
            clump_graph.add_edge(i, j)
    return nx.connected_components(clump_graph)


def simplify_junctures(graph, epsilon=6):
    """Simplifies clumps by replacing them with a single juncture node. For
    each clump, any nodes within epsilon of the clump are deleted. Remaining
    nodes are connected back to the simplified junctures appropriately."""
    graph = graph.copy()
    max_quadrance = epsilon * epsilon
    clumps = find_clumps(graph, epsilon)
    
    for clump in clumps:
        to_delete = set([])
        for node in graph.nodes_iter():
            if graph.degree(node) > 2:
                for juncture in clump:
                    #d = graph.graph[node]['d']*1.3
                    #d*=d
                    if quadrance(node, juncture) < max_quadrance:
                        to_delete.add(node)
        
        to_join = set([])
        for node in to_delete:
            for neighbor in nx.all_neighbors(graph, node):
                if not (neighbor in to_delete):
                    to_join.add(neighbor)
        
        clump_center = (0, 0)
        for juncture in clump:
            clump_center = (
                clump_center[0]+juncture[0], clump_center[1]+juncture[1])
            
        clump_center = (
            clump_center[0] / len(clump), clump_center[1] / len(clump))
                
        for node in to_delete:
            graph.remove_node(node)
        
        for node in to_join:
            graph.add_edge(node, clump_center)
    
    return graph

def simplify_graph(G, eps=3):
    ''' Simplifies all branches of graph using the Ramer-Douglas-Peucker algorithm'''
    
    G = G.copy()
    
    dist = G.graph['dist']

    branches = skeleton_branches(G)
    for branch in branches:
        branch_prime = rdp(branch, eps)
        for n in branch[1:-1]:
            G.remove_node(n)
        for a, b in zip(branch_prime, branch_prime[1:]):            
            G.add_edge(a, b)
        
        # add info
        for x,y in branch_prime:
            G.node[(x,y)]['d'] = dist[y,x]
            G.node[(x,y)]['p'] = v2(x,y)
    return G


########################################################
### 
def remove_spurious(G, thresh=1.5):
    ''' Radius check for spurious branches, 
        removes branches that are  shorter than a given ratio of 
        the radius at the fork point'''
    P, D = graph_positions_and_radii(G)
    B = extremity_branches(G)
    
    for branch in B:
        #print branch

        bl = distance_int(branch[0], branch[-1])
        w = max(D[branch[0]], D[branch[-1]])
        
        if w > 0.0001 and bl/w < thresh:
            remove_branch(G, branch)
    
    for n in G.nodes():
        if G.degree(n) < 1:
            G.remove_node(n)

    #G = nx.connected_components(G)

    return G


def graph_positions_and_radii(G):
    P = {}
    D = {}
    for n in G.nodes():
        x, y = n
        P[n] = np.array([float(x),float(y)])
        D[n] = G.node[n]['d']
        
    return P, D

def closest_point_on_segment( p, a, b ):
    v = b-a
    w = p-a
    
    d1 = np.dot(w,v)
    if d1 <= 0.0:
        return a
    d2 = np.dot(v,v)
    if d2 <= d1:
        return b
    
    t = d1/d2
    return a + v*t


def von_mises( theta, kappa, mu=0.0 ):
    ''' Von Mises distribution centered at mu and with spread kappa'''
    return np.exp( kappa * np.cos(theta-mu) ) / (np.pi*2.0*np.i0(kappa))

def gaussian( x, mu, sigma):
    v = (x-mu)/sigma
    v*=v
    return (1.0/(sigma*np.sqrt(np.pi*2)))*np.exp(-0.5*v)

def is_axial(G, branch):
    ''' An axial branch has one end with degree 1 and one end with degree > 2'''
    if G.degree(branch[0]) == 1 and G.degree(branch[-1]) > 2:
        return True

    if G.degree(branch[-1]) == 1 and G.degree(branch[0]) > 2:
        return True
        
    return False
    
def axial_branch_indices(G, branches):
    ''' Returns the indices of all branches that are axial'''

    axial = []    
    for i in range(len(branches)):
        if is_axisal(G, branches[i]):
            axial.append(i)
    
    return axial
    
def skel_prior_feldman(branch_ctrs):
    for C in ctrs:
        A = turning_angles(C)
    return np.prod(A)

    

def shape_likelyhood(ctrs, branch_ctrs, skip=[], debug_draw=True):
    if debug_draw:
        plt.figure()
        for b in branch_ctrs:
            plt_skeleton_branch(b, 3.0, 'r')
            
    for C in ctrs: 
        if debug_draw:
            plt.plot(C[0,:], C[1,:], 'g')
        
        for i in range(C.shape[1]):
            p = C[:,i]
            closest = None
            min_d = 0.0
            for j in range(len(branch_ctrs)):
                if j in skip:
                    continue
                Pb = branch_ctrs[j]
                for k in range(Pb.shape[1]-1):                    
                    pb = closest_point_on_segment(p, Pb[:,k], Pb[:,k+1])
                    d = np.dot(p-pb, p-pb)
                    if closest == None or d < min_d:
                        min_d = d
                        closest = pb
            if debug_draw:
                plt_line(p, closest, 'c')
            
    if debug_draw:
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()
        
def density(G, skip=None):
    """ method to calculate the density of a graph """
    V = len(G.nodes())
    E = len(G.edges())
    if skip!=None:
        V -= len(skip)
        E -= len(skip)/2
    return 2.0 * E / (V *(V - 1))


def reconstruct_shape(G, skip=None):
    w, h = G.graph['img_size']
    img = np.zeros((h,w)).astype(np.uint8)
    
    for n in G.nodes():
        if skip!=None and n in skip:
            continue
        cv2.circle(img, n, int(G.node[n]['d']),(255,255,255), -1)
    return img
#np.random.uniform(0.8,1.2
    
def shape_area(img):
    return cv2.countNonZero(img)
    
def img_diff(a, b):
    return np.maximum(0, a - b)
    
def remove_extremity_branch(G, branch):
    ''' remove an extremity branch from a graph,
        expects the branch to start with a node of degree 1'''
    for n in branch[:-1]:
        G.remove_node(n)
    
    if G.degree(branch[-1]) == 0:
        G.remove_node(branch[-1])
        
def skeleton_length(G, skip=None):
    l = 0.0
    for a, b in G.edges():
        if skip!=None and a in skip and b in skip:
            continue
        l += distance_int(a, b)
    return l
    
def diff_area(img, G, skip=None):    
    r_img = reconstruct_shape(G, skip)
    diff_img = img_diff(img, r_img) 
#    plt.figure()
#    plt.imshow(diff_img)
#    plt.show()
#    
    a = float(shape_area(diff_img))
    return a #float(shape_area(diff_img))


def prune_skeleton(img, G, alpha=1.9): #0.02):
    ''' Based on Skeleton pruning as trade-off between skeleton simplicity and reconstruction error
        Shen et al. 2012
        http://cis-linux1.temple.edu/~latecki/Papers/OptimalSkeletonScienceChina2013.pdf '''
    
    debug_print( 'Pruning skeleton' )
    area_orig = shape_area(img)
    len_orig = skeleton_length(G)
    
    def cost(G, skip):
        #return np.log(skeleton_length(G, skip) + 1) #np.log(density(G, skip)+1.0)
        if skip == None:
            skip = []
        #return np.log(skeleton_length(G, skip) / len_orig + 1)
        return alpha*((diff_area(img, G, skip)/area_orig)) +  np.log(skeleton_length(G, skip)/len_orig + 1)
    
    E = extremity_branches(G)
        
    S = [ {'G':G, 'cost':cost(G, None)} ]
    G_prime = G.copy()
    it = 1
    while len(E) > 2:
        
        it += 1
        
        W = []
        for e in E:
            w = cost(G_prime, e)
            W.append({'branch':e, 'w':w})
        
        #print [w['w'] for w in W]
        Wmin = min(W, key=lambda v: v['w'])
        #print Wmin['w']
        G_prime=G_prime.copy()
        remove_extremity_branch(G_prime, Wmin['branch'])
        S.append( {'G':G_prime, 'cost':Wmin['w']} )
        
        debug_print( 'Prune iteration ' + str(it) )
        debug_print( 'Cost: ' + str(Wmin['w']) )
        debug_print( '|G|: ' + str(len(G_prime.nodes())) )
        E = extremity_branches(G_prime)
    
    best = min(S, key=lambda v: v['cost'])
    return best['G'], best['cost']



# build stroke graph
def stroke_key(S, s):
    ''' Key for a stroke'''
    l = len(s)
    
    nodes = S.nodes()
    ox, oy = 0, 0
    ok = False
    while ok==False:
        # short stroke, get mid point along segment
        if l == 2:
            key = ((s[0][0]+s[1][0])/2 + ox,
                     (s[0][1]+s[1][1])/2 + oy)
        else:
            # return mid point
            i = (l-1)/2
            key = (s[i][0]+ox, s[i][1]+oy)
        
        # make sure we don't have a overlap
        if key in nodes:
            ok = False
            ox = ox+int((np.random.uniform()-0.5)*4)
            oy = oy+int((np.random.uniform()-0.5)*4)
        
        else:
            ok = True
        
    return key
    
def order_strokes(f, B1, B2, C1, C2):
    ''' Orders the points of two strokes (stroke nodes and contour) around a fork point
        so the sequence is B1[0]...B1[end] f [B2[0]...B2[end]'''
    # order appropriately
    b1, b2 = B1[::], B2[::]
    
    ok = True
    if B1[0] == f:
        B1 = B1[::-1]
        C1 = np.fliplr(C1)
    elif B1[-1] == f:
        pass
    else:
        ok=False
        
    if B2[-1] == f:
        B2 = B2[::-1]
        C2 = np.fliplr(C2)
    elif B2[0] == f:
        pass
    else:
        ok=False
    if not ok:
        debug_print( 'Not ok!!!!!' )
    return B1, B2, C1, C2
    

def continuity(S, f, s1, s2):
    def mean_support(start, size, C):
        return np.mean([C[:,i] for i in range(start, size, np.sign(size))], axis=0)
        
    ''' Chacks for continuity between s1 and s2 around fork point f'''
    C1 = S.node[s1]['ctr']
    C2 = S.node[s2]['ctr']
    B1 = S.node[s1]['stroke']
    B2 = S.node[s2]['stroke']
            
    B1, B2, C1, C2 = order_strokes(f, B1, B2, C1, C2)
    
    support_size = 30 #pixels    
    # make sure we don't overflow    
    support_size = min(support_size, C1.shape[1])
    support_size = min(support_size, C2.shape[1])
    
    # TODO avareage angles up to support to remove noise.
    try:
        p1 = mean_support(-1,-support_size, C1) # C1[:,-support_size]
        p2 = C1[:,-1]
        p3 = C2[:,0]
        p4 = mean_support(0,support_size, C2) #C2[:,support_size-1]
    except:
        print 'Error in continuity comp'
        raise ValueError
#    plt.figure()
#    nx.draw(G_prime, pos=P, node_shape='', color='g')
#    #plt_arrow(p1, p2, 5, color='r')
#    #plt_arrow(p3, p4, 5, color='g')
#    plt_line(p1, p2,  color='r')
#    plt_line(p3, p4,  color='g')
#    plt.gca().invert_yaxis()
#    plt.gca().set_aspect('equal', adjustable='box')
#    plt.show()
    
    theta = abs( degrees( angle_between(p4-p3, p2-p1) ) )
    #print theta
    
    return theta
        

def stroke_graph_positions(S):
    P = {}
    for n in S.nodes():
        P[n] = v2(n[0],n[1])
    return P
   
def is_juncture(S, n):
    return 'C' in S.node[n] # degree(n) > 2
    
def check_continuous(S, f, a, b):
    if a in S.node[f]['C']:
        if S.node[f]['C'][a]==b:
            return True
    if b in S.node[f]['C']:
        if S.node[f]['C'][b]==a:
            return True
    return False
    
def make_loop_key(S, f):
    ok = False
    ox, oy = 1,1
    key = f 
    while ok==False:
        ox = ox+int((np.random.uniform()-0.5)*2)
        oy = oy+int((np.random.uniform()-0.5)*2)        
        key = (key[0]+ox, key[1]+oy) 
        if not key in S.nodes():
            ok=True
    return key
    
def build_stroke_graph(G, cont_eps=40):
    S = nx.Graph()
    
    # distance info
    S.graph['dist']=G.graph['dist']
    
    # first find junctures
    F = find_junctures(G)
    for f in F:
        S.add_node(f)
        # continuity
        S.node[f]['C'] = {}
        # termination
        S.node[f]['T']=[]
        
    strokes = skeleton_branches(G)
    # remove corrupt strokes
    strokes = [s for s in strokes if len(s) > 1]
    
    stroke_keys = [stroke_key(S, s) for s in strokes]    
    
    n_strokes = len(strokes)
    
    # add strokes to graph
    for i in range(n_strokes):
        key = stroke_keys[i]        
        S.add_node(key)
        S.node[key]['loop']=False
        S.node[key]['stroke']=strokes[i]
        S.node[key]['ctr']=uniform_sample(branch_to_contour(strokes[i]),1)

        
    # connect junctures    
    F = find_junctures(G)
    for f in F:
        for i in range(n_strokes):
            s = strokes[i]
            key = stroke_keys[i]
            if f==s[0] or f==s[-1]:
                if S.has_edge(f, key):
                    ## TODO: Dodgy, need to find a better way to handle loops
                    debug_print( 'Found loop' )                     
                    S.node[key]['loop']=True
                    S.add_edge(f, key)
                else:   
                    S.add_edge(f, key)
    
    # check for continuity
    def remove_from_values(f, key):
        S.node[f]['C'] = { k:v for k,v in S.node[f]['C'].iteritems() if v != key }
                    
    
    # for each node -> get angles with other ones
    # needs to be mutually exclusive 
    # so if a <-> b and c <-> b 
    # could sort 
    for f in F:
        adj = S.neighbors(f)
        # adds min continuous pairs
        pairs = itertools.combinations(adj,2)
        combs = [ (a, b, continuity(S, f, a, b)) for a, b in pairs]
        combs.sort(key=lambda v:v[2])
        for n in adj:
            for a, b, cont in combs:
                if n==a or n==b:
                    if cont < cont_eps:
                        S.node[f]['C'][a] = b
                        S.node[f]['C'][b] = a
                    continue
        
                                    
#            if continuity(S, f, a, b) < cont_eps:
#                remove_from_values(f, a)
#                remove_from_values(f, b)
#                                
#                S.node[f]['C'][a] = b
#                S.node[f]['C'][b] = a
                
        
         #selects smallest angle that satisfies continuity constraint
#        for a in adj:
#            if a in S.node[f]['C']:
#                continue
#            
#            cont = []
#            
#            for b in adj:
#                if a != b:
#                    cont.append([b, continuity(S, f, a, b)])
#            min_c = min(cont, key=lambda v: v[1])
#            if min_c[1] < cont_eps and not min_c[1] in S.node[f]['C']:
#                S.node[f]['C'][a]=min_c[0]
#                S.node[f]['C'][min_c[0]]=a
                                
        # test pairwise for each combination of strokes
#        pairs = itertools.combinations(adj,2)
#        
#        for a, b in pairs:
#            if is_continuous(S, f, a, b):
#                if not check_continuous(S, f, a, b):
#                    S.node[f]['C'][a] = b
#        for a in adj:
#            cont = []
#            for b in adj:
#                if a != b:
#                    cont.append([b, continuity(S, f, a, b)])
#            min_c = min(cont, key=lambda v: v[1])
#            if min_c < cont_eps:
#                S.node[f]['C'][a]=b
#                
        for n in adj:
            if not n in S.node[f]['C'].keys():
                S.node[f]['T'].append(n)
                                
    return S
    
def terminal_nodes(S):
    term = {}
    for n in S.nodes():
        if is_juncture(S, n):
            for t in S.node[n]['T']:
                term[t]=1
    return [ key for key, value in term.iteritems() ]
    
def terminates_in(S, n, f):
    if not is_juncture(S, f): 
        return False
    return n in S.node[f]['T']

def is_junction(S, n):
    return 'C' in S.node[n]
    
def check_path(S, P):
    l = len(P)
    
    if l == 2:
        return True
            
    if l < 3:
        return False
    
    for i in range(1,l-1):
        n = P[i]
        a = P[i-1]
        b = P[i+1]
        is_junct = is_juncture(S, n)
        ok = (S.degree(n)==2 and not is_junct) or (is_junct and check_continuous(S, n, a, b))
        if not ok:
            return False
            
    return True

def extract_strokes(S, G):
        
    strokes = []
    
    debug_print( 'Finding isolated strokes' )
    # find isolated strokes:
    for n in S.nodes():
        if S.degree(n)==0:
            strokes.append([n])
            
    debug_print( 'Finding short terminal strokes' )
    
    # find nodes that are terminal on both sides
    term = terminal_nodes(S)
    for t in term:
        if S.degree(t) == 2:
            a,b = S.neighbors(t)
            if terminates_in(S, t, a) and  terminates_in(S, t, b): #t in S.node[a]['T'] and t in S.node[n]['T']:
                strokes.append([t])

    # find terminal nodes with degree 1
    for t in term:
        if S.degree(t) == 1:
            strokes.append([t])
            
    debug_print( 'found ' + str(strokes) )
                
    # find end strokes 
    ends = [n for n in S.nodes() if S.degree(n)==1]
    ends += term
    
    plt.figure()
    P, D = graph_positions_and_radii(G)
    
    for n in ends:
        p = v2(*n)
        plt_circle(p, 5, color='c', fill=True)
        plt.text(p[0]+10,p[1],str(S.degree(n)))
    nx.draw(G, pos=P, node_shape='', color='g')
    
    plt.gca().invert_yaxis()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()
    
    debug_print( 'Will search ' + str(ends) )
    
    # all simple paths 
    pairs = itertools.combinations(ends,2)
    
    debug_print( 'All combinations: ' )
    debug_print( pairs )
    
    paths = []
    for a,b in pairs:
        paths += nx.all_simple_paths(S, a, b)
    
    if False: # debug draw simple paths
        for path in paths:        
            
            plt.figure()
            nx.draw(G_prime, pos=P, node_shape='', color='g')
            ctr = branch_to_contour(path)
            plt.plot(ctr[0,:], ctr[1,:], color='r')
            plt.gca().invert_yaxis()
            plt.gca().set_aspect('equal', adjustable='box')
            plt.show()
    
    debug_print( 'Checking valid simple paths:' )
    # check all paths if valid
    for P in paths:
        if check_path(S, P):
            strokes.append(P)

    # check duplicate strokes and subsets 
    N = len(strokes)
    trash = []
    Ps = [set(s) for s in strokes]
    for i in range(N):
        for j in range(N):
            if i==j:
                continue
            if Ps[j].issubset(Ps[i]) and j not in trash:
                trash.append(j)
    
    # must be more pythonic way to do this...
    strokes = [strokes[i] for i in range(N) if i not in trash]
    
    # merge contiguous sub-stroke sequences
    debug_print( 'Reordering sub-strokes' )
    for seq in strokes:
        if len(seq) == 1:
            continue
        N = len(seq)
        for i in range(N):
            n = seq[i]
            if is_juncture(S, n) and i < N-1 and i > 0:
                s1 = seq[i-1]
                s2 = seq[i+1]
                C1 = S.node[s1]['ctr']
                C2 = S.node[s2]['ctr']
                B1 = S.node[s1]['stroke']
                B2 = S.node[s2]['stroke']
                B1_, B2_, C1_, C2_ = order_strokes(n, B1, B2, C1, C2)
                S.node[s1]['ctr'] = C1_
                S.node[s2]['ctr'] = C2_
                S.node[s1]['stroke'] = B1_
                S.node[s2]['stroke'] = B2_

    merged = []                
    
    debug_print( 'Merging sub-strokes' )
    for seq in strokes:
        if not seq:
            continue

        stroke = []
        
        for n in seq:
            if is_juncture(S, n): #S.degree(n) > 2:
                continue
            
            B = S.node[n]['stroke']
            if stroke and stroke[-1]==B[0]:
                stroke.pop()
            stroke = stroke + B
    
        merged.append(stroke)

    return merged
    
def merged_strokes_radii(S, merged):
    dist = S.graph['dist']
    res = []
    for stroke in merged:
        D = np.zeros(len(stroke))
        for i in range(len(stroke)):
            n = stroke[i]
            D[i] = dist[n[1], n[0]]
        res.append(D)
    return res
    
def merged_strokes_to_contours(S, merged):
    res = []
    for stroke in merged:
        ctr = np.zeros((2, len(stroke)))
        for i in range(len(stroke)):
            ctr[:,i] = v2(*stroke[i])# + (np.random.uniform(size=2)-0.5)*10
        res.append(ctr)
    return res
    



#Trix:
# Elitims-Truncation:
#    N fittest ind. are selected
#    Elitism -> exact copy goes into new gen
#        age constraint -> remove after M generations
#    Truncation -> top individuals have children
#    (May coverge early due to limited gene pool)
# 
# Fittness Propportionate selection
#    Select n with a prob p(i) proportionate to fittness
#        p(i) = f(i) / sum(f(j))
#    gives unfit ind. chance of selection
#

#########################################################

def cumdiv(X):
    X = np.array(X)
    X[1:] = 1.0/X[1:]
    return np.prod(X)
    
def invsort(X):
    return -np.sort(-np.array(X))
    

def length_score(strokes, X):
    ''' Gives a highest score to an ordering from longest to shortest'''
    #if not hasattr(length_score, 'min'):
    #    length_score.min = cumdiv(np.sort([chord_length(ctr) for ctr in strokes]))
    #    length_score.max = cumdiv(invsort([chord_length(ctr) for ctr in strokes]))
        
    #v = (cumdiv([chord_length(strokes[i]) for i in range(len(strokes))])-length_score.min)/(length_score.max-length_score.min)
    v = cumdiv([chord_length(strokes[i]) for i in range(len(strokes))])*1000
    return v
    
def stroke_direction_score(stroke, theta, ordering):
    D = np.diff(stroke, axis=1)
    if ordering==1:
        D = -D
    A = np.arctan2(D[1,:], D[0,:])
    a = np.sum(A) / A.shape[0]
    return von_mises(a, np.pi/2, theta)
    
def direction_fitness(strokes, X):
    n = X.shape[0]
    p = 0.0

    for i, order in X:
        p += stroke_direction_score(strokes[i], np.pi/2, order) # strokes to the right ( maybe left better?)
        p += stroke_direction_score(strokes[i], np.pi, order) # down-strokes 
    return p / (2*n)
    
            
def closeness_fitness(strokes, X, d_range):
    d_range = d_range*d_range
    p = 0.0
    n = X.shape[0]
    for i in range(n-1):
        a, order_a = X[i]
        b, order_b = X[i+1]
        sa = strokes[a]
        sb = strokes[b]
        pa = sa[:,-1] if order_a==0 else sa[:,0] 
        pb = sb[:,0] if order_b==0 else sb[:,-1] 
        
        d = np.dot(pa-pb, pa-pb)
        
        p += np.exp(-d / d_range) #dgaussian(d, 0.0, d_range)
    return p/n

def objective(strokes, X):
    return (closeness_fitness(strokes, X, 100.0)) # / 2 # + length_score(strokes, X))/3
#    return length_score(strokes, X) # closeness_fitness(strokes, X, 150.0) #direction_fitness(strokes, X)
#(direction_fitness(strokes, X) + 
def gen_population(n_strokes, n):
    X = range(n_strokes)
    Y = []
    return [ np.c_[np.random.permutation(X), vflip(n_strokes)]  
                for i in range(n) ]

def fittness( strokes, P ):
    return [ objective(strokes, X) for X in P ]
    
def selection( P, F ):
    # sort     
    I = np.argsort(-np.array(F))
    P = [P[i] for i in I]
    F = [F[i] for i in I]
    
    max_fit = F[0]
    mean_fit = np.mean(F)
    
    pool = []
    
    for i in range(len(F)):
        f = F[i]
        e = f / mean_fit
        n = np.floor(e)
        p = e-n
        if flip(p):
            n = n+1
        
        for j in range(int(n)):
            pool.append(i)
    
    
    return P, F, pool

def indexof(X, v):
    return np.where(X==v)[0][0]
    
            
def pmx(a,b,n):
    a = np.array(a)
    for i in range(n):
        j = indexof(a, b[i])
        a[j] = a[i]
        a[i] = b[i]
    return a
    
def crossover( mom, dad ):
    n = mom.shape[0]
    c = n/2
    # pmx for ordering    
    seq = pmx(mom[:,0], dad[:,0], c)
    order = np.array(mom[:,1])
    order[:c] = dad[:c,1]
    
    return np.c_[seq, order]
   
def mutate(X, p):
    X = np.array(X)
    if flip(p):
        X[:,0] = np.random.permutation(X[:,0])
    if flip(p):
        X[:,1] = vflip(X.shape[0])
    return X
    
def fliph(X):
    return np.array([c for c in reversed(X.T)]).T
    
def reproduction( P, F, pool, keep_best=1 ):
    P_new = np.array(P)
    
    for i in range(keep_best, len(P)):        
        m = np.random.choice(pool)
        d = np.random.choice(pool)

        mom = np.array(P[m])
        dad = np.array(P[d])
        
        child = crossover(mom, dad) if flip() else crossover(dad, mom)
        child = mutate(child, 0.1) #1.0-(F[m]+F[d])/2)                
        P_new[i] = np.array(child)
    
    return P_new
    
def optimize_stroke_orders( ctrs, strokes, radii=None, n_iter=100 ):
    # generate 
    P = gen_population( len(ctrs), 100 )
    
    for i in range(n_iter):
        F = fittness(ctrs, P)
        P, F, pool = selection(P, F)
        debug_print( 'Score = ' + str(F[0]) )
        P = reproduction(P, F, pool)
    
    F = fittness(ctrs, P)
    P, F, pool = selection(P, F)        
    
    # reordered strokse
    best = P[0]
    
    # ordered strokes (nodes)
    s_prime = []
    # ordered contours (position, radii)
    c_prime = []
    for i, o in best:
        c = np.array(ctrs[i])  
        s = strokes[i][::]
        
        if radii != None:
            r = np.array(radii[i])
        if o:
            c = fliph(c)
            s = s[::-1]
            if radii != None:
                r = fliph(r)
            
        if radii != None:
            c = np.vstack([c, r])
        c_prime.append(c)
        s_prime.append(s)
        
    return c_prime, s_prime
    
def extract_stroke_junctures(S, s_prime):
    ''' For each juncture node in the graph, adds a list of
        tuples in the form (index_of_stroke, index_of_point_in_stroke)'''
        
    J = [n for n in S.nodes() if is_juncture(S,n)]
    Sj = []
    for junct in J:
        js = []
        for i in range(len(s_prime)):
            for j in range(len(s_prime[i])):
                if s_prime[i][j]==junct:
                    js.append((i,j))
        if len(js):
            Sj.append(js)
    return Sj
    
    
def parse_image( path, simplify_eps=1 ):
    ''' Skeletonization proc '''
    
    # read img
    img = cv2.imread(path,-1) #.astype(np.uint8)
    img = (img[:,:,3] > 128).astype(np.uint8)*255

    h,w = img.shape

    #if config.debug_draw:
    plt.figure()
    plt.imshow(img,'gray', interpolation='nearest')
    plt.show()
    
    ####################################
    ### Medial Axis
    G = compute_ma( img )

    plt.figure()
    plt.imshow(G.graph['dist'],'gray', interpolation='nearest')
    plt.show()

    if config.debug_draw:
        fig=plt.figure(1)
        plt.title('Medial Axis')
        plt.axis([0,w,0,h])
        ax=fig.add_subplot(1,1,1)
        P, D = graph_positions_and_radii(G)
        nx.draw(G, pos=P, node_shape='', color='g')
        ax.invert_yaxis()
        ax.set_aspect('equal', adjustable='box')
        plt.show()

    ####################################
    ### Process MA
    G_prime, cost = prune_skeleton(img, G)
    # also disk pass     
    G_prime = remove_spurious(G_prime)
    # DP simplify
    G_prime = simplify_graph(G_prime, eps=simplify_eps)
    update_attributes(G_prime)
    
    P, D = graph_positions_and_radii(G_prime)
    
    if config.debug_draw:
        plt.figure()
        plt.title('Processed MA')
        nx.draw(G_prime, pos=P, node_shape='', color='g')
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()
    
    ####################################
    #### Stroke exctraction
    S = build_stroke_graph(G_prime)
    merged = extract_strokes(S, G_prime)
    
    
    ctrs = merged_strokes_to_contours(S, merged)
    
    if config.debug_draw:
        P2 = stroke_graph_positions(S)
        
        # draw 
        plt.figure()
        plt.title('Sub-stroke adjacency graph')
        nx.draw(G_prime, pos=P, node_shape='', color='g')
        nx.draw(S, pos=P2, node_shape='', edge_color='r')
        
        for n in S.nodes():
            p = v2(*n)
            if is_juncture(S, n):
                plt_circle(p, 5, color='r', fill=True)
            else:
                plt_circle(p, 2, color='g', fill=True)
                #plt.gca().text(p[0]+10,p[1],str(S.degree(n)))
                
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

        # draw color coded contours
        
        plt.figure()
        plt.title('Sub-strokes')
        for ctr in ctrs:
            plt.plot(ctr[0,:], ctr[1,:])    
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    
    
    if True: #config.debug_draw:
        radii = merged_strokes_radii(S, merged)
        plt.figure()
        plt.title('Reconstruction')
        for ctr, D in zip(ctrs, radii):
            P, D = uniform_sample(ctr, 3, data=D)
            plt.plot(P[0,:], P[1,:])
        #    for p in P.T:
        #        plt_circle(p, 15, color='m', fill=True)
            for p,d in zip(P.T, D):
                plt_circle(p, d, color='k', fill=True)
            #plt.plot(ctr[0,:], ctr[1,:])    
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    ####################################
    ### Optimize stroke ordering
    radii = merged_strokes_radii(S, merged)

    c_prime, s_prime = optimize_stroke_orders( ctrs, merged, radii )
    
    # Extract junctures, we will use these as a constraint in the visualization
    #junct = extract_junctures(S, merged)
    Sj = extract_stroke_junctures(S, s_prime)
    
    
    if True: #config.debug_draw:
        plt.figure()
        i = 1
        for ctr in c_prime:
            P = ctr 
            plt.plot(P[0,:], P[1,:], label=str(i))
            plt_arrow(P[:,-2], P[:,-1], 10)
            #plt.gca().text(P[0,0],P[1,0],str(i))
            i = i+1
        #    for p in P.T:
        #        plt_circle(p, 15, color='m', fill=True)
            #for p,d in zip(P.T, D):
            #    plt_circle(p, d, color='k', fill=True)
            #plt.plot(ctr[0,:], ctr[1,:])    
        for js in Sj:
            for i, j in js:
                plt_circle(c_prime[i][:,j], 10, color='c')
                x,y,z = c_prime[i][:,j]
                plt.text(x+5,y,str(len(js)))
                
        plt.gca().invert_yaxis()
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()
    
    return G_prime, c_prime, Sj



def load_arrays(path, delimiter=',', array_delimiter=';'):
    f = open(path,'r')
    s = f.read()
    if not s:
        return None
    A = arrays_from_string(s, delimiter, array_delimiter)
    f.close()
    return A

def save_graph(path, G):
    radii = np.array([G.node[n]['d'] for n in G.nodes()])
    nodes = np.array(G.nodes())
    gnodes = G.nodes()
    edges = np.array([(gnodes.index(a), gnodes.index(b)) for a,b in G.edges()])
    
    save_arrays(path, [nodes, edges, radii])

# curve one d object ( eg. paramterized by t )
def save_stroke_junctures(path, Sj):
    Sj = [np.array(js) for js in Sj]
    save_arrays(path, Sj)
    
def load_graph(path):
    G = nx.Graph()
    nodes, edges, radii = load_arrays(path)
    
    for n in nodes:
        G.add_node((int(n[0]), int(n[1])))
        
    for e in edges:
        a, b = int(e[0]), int(e[1])
        G.add_edge(tuple(nodes[a]), tuple(nodes[b]))
    print nodes
    print G.nodes()
    print G.edges()
    return G
    
def save_strokes(path, S):
    save_arrays(path, S)
    
def load_strokes(path):
    return load_arrays(path)
    

if __name__ == '__main__':
    print 'Reading image'
    import os
#    imgs = []
#    path = '../skeletor_visualizer/bin/data/
#    for file in os.listdir("../skeltondata"):
#        if file.endswith(".png"):
#            imgs.append('data/'+file) #print(file)
#    print imgs
#    
    #save_graph('G.txt', G)
    #gg = load_graph('G.txt')
    
#    Pg = {n:v2(*n) for n in gg.nodes()}
#    plt.figure()
#    nx.draw(gg, pos=Pg, node_shape='', color='g')
#    plt.gca().invert_yaxis()
#    plt.gca().set_aspect('equal', adjustable='box')
#    plt.show()
#         
#            
    G, S, Sj = parse_image('./test.png', simplify_eps=3)
    
