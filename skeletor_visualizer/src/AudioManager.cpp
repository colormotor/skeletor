#ifndef TARGET_LINUX
//
//  AudioManager.cpp
//  VolumeRunnerApp
//
//  Created by Daniel Berio on 11/27/14.
//
//

#include "AudioManager.h"
#include "ofxImGui.h"

//This shows how to use maximilian to build a polyphonic synth.
#define N_SNDS 20

//These are the synthesiser bits
maxiOsc VCO1[N_SNDS],VCO2[N_SNDS],LFO1[N_SNDS],LFO2[N_SNDS];
maxiFilter VCF[N_SNDS];
maxiEnvelope ADSR[N_SNDS];

//(double input, double attack, double decay, double sustain, double release, long holdtime, int trigger
//These are the control values for the envelope
double adsrEnv[8]={1,5,0.325,200,0.325,400,0,1000};

//This is a bunch of control signals so that we can hear something
maxiOsc timer;//this is the metronome
int currentCount,lastCount,voice=0;//these values are used to check if we have a new beat this sample

//and these are some variables we can use to pass stuff around
double VCO1out[N_SNDS],VCO2out[N_SNDS],LFO1out[N_SNDS],LFO2out[N_SNDS],VCFout[N_SNDS],ADSRout[N_SNDS],mix,pitch[N_SNDS];

float note1=40;
float note2=40;
float lfo1=3.49;
float lfo2=3.104;
float vco=11.159;
float chord=3;
float nn_params[10] = {1.0,1.0,1.0,1.0,1.0,
    1.0,1.0,1.0,1.0,1.0};
float pw1=0.113;
float pw2=0.207;
float pmul=1.285;

int fibbo[20]={A,A,B,C,E,C,C,A,D,E,D,D,C,B,E,B,B,D,A,E};

AudioManager::AudioManager()
:
stepPhase(0.0),
resonance(3.7),
cutoff(244.7)
{
    
    startThread();
}

void AudioManager::stop()
{
    stopThread();
}

void AudioManager::setup()
{
    
}

void AudioManager::gui()
{
    if (ImGui::CollapsingHeader("Audio Shit")) {
        ImGui::SliderFloat("Resonance", &resonance, 0.1, 20.0);
        ImGui::SliderFloat("Cutoff", &cutoff, 50.0,1000.0);
        ImGui::SliderFloat("note1", &note1, 1.0,40.0);
        ImGui::SliderFloat("note2", &note2, 1.0,40.0);
        ImGui::SliderFloat("lfo1", &lfo1, 0.1,4.0);
        ImGui::SliderFloat("lfo2", &lfo2, 0.1,4.0);
        ImGui::SliderFloat("pw1", &pw1, 0.1,2.0);
        ImGui::SliderFloat("pw2", &pw2, 0.1,2.0);
        ImGui::SliderFloat("pmul", &pmul, 0.1,4.0);
        
        ImGui::SliderFloat("vco", &vco, 0.1,12.0);
        ImGui::SliderFloat("chord", &chord, 2,18.0);
        
        char foo[256];
        for( int i = 0; i < 10; i++ )
        {
            sprintf(foo,"p %d",i);
            ImGui::SliderFloat(foo, &nn_params[i], 0.0,1.0);
        }
        
    }
}

void AudioManager::update()
{
    double t = ofGetElapsedTimeMillis();
    lock();
    DelayedNote * cur = delayedNotes.head;
    while(cur)
    {
        DelayedNote * dn = cur;
        cur = cur->next;
        if((t-dn->t) > 300)
        {
            delayedNotes.remove(dn);
            notes.push_back(dn->note);
            delete dn;
        }
    }
    unlock();
}

void AudioManager::playNote( int note )
{
    DelayedNote * dn = new DelayedNote(note,ofGetElapsedTimeMillis());
    delayedNotes.insert(dn);
}

int cur_base=0;
float freq=30.0;
// Just adapted the maximilian PolySynth example here
void AudioManager::play( double * output )
{
    static bool playing=false;
    mix=0;//we're adding up the samples each update and it makes sense to clear them each time first.
    
    //so this first bit is just a basic metronome so we can hear what we're doing.
    
    currentCount=(int)timer.phasor(0.1);//this sets up a metronome that ticks 8 times a second
    if(currentCount!=lastCount)
    {
        cur_base=(cur_base+1)%5;
        lastCount=0;
    }
    
    //printf("C %d\n",currentCount);
    
    lock();
    
    //for( int i = 0; i < N_SNDS; i++ )
    //    pitch[i]=120;
    
    for( int i = 0; i < notes.size(); i++ )
    {
        voice = voice%4;
        ADSR[10+voice].trigger(0, adsrEnv[0]);//trigger the envelope from the start
        pitch[10+voice]=notes[i];//voice+1;
        voice++;
        notes.clear();
    }
    
    //static float p=30.0;
    float start_freq[5]={120,20,15,4,40};
    float note = fibbo[cur_base%20] + OCT*2;
    //freq += pitch_to_frequency(fibbo[cur_base%20]);   (start_freq[cur_base%5] - freq)*0.00001;//10.0;
    float p = freq;
    for(int i = 0; i < 10; i++)
    {
        if(playing==false)
            ADSR[i].trigger(0, adsrEnv[0]);//trigger the envelope from the start
        pitch[i]=pitch_to_frequency(note); //p;//voice+1;
        note += chord;
    }
    
    //playing=true;
    unlock();
    
    /*
     if (lastCount!=currentCount) {//if we have a new timer int this sample, play the sound
     
     if (voice==6) {
     voice=0;
     }
     
     ADSR[voice].trigger(0, adsrEnv[0]);//trigger the envelope from the start
     pitch[voice]=voice+1;
     voice++;
     
     lastCount=0;
     
     }*/
    
    //and this is where we build the synth
    double v;
    for (int i=0; i<N_SNDS; i++) {
        float cut=cutoff;
        float vol=1.0;
        if(i>=10)
        {
            cut*=3;
            vol*=2.5;
        }
        
        ADSRout[i]=ADSR[i].line(8,adsrEnv);//our ADSR env has 8 value/time pairs.
        
        LFO1out[i]=LFO1[i].sinebuf(vco);//this lfo is a sinewave at 0.2 hz
        VCO1out[i]=VCO1[i].pulse(pitch[i]+LFO1out[i]*20,pw1);//here's VCO1. it's a pulse wave at 55 hz, with a pulse width of 0.6
        VCO2out[i]=VCO2[i].pulse((pitch[i]+pmul)+LFO1out[i]*2,pw2);//here's VCO2. it's a pulse wave at 110hz with LFO modulation on the frequency, and width of 0.2
        //v = VCO1out[i];
        VCFout[i]=VCF[i].lores((VCO1out[i]+VCO2out[i]), cutoff, resonance); //+((pitch[i]+LFO1out[i])*500), resonance);//now we stick the VCO's into the VCF, using the ADSR as the filter cutoff
        
        v = (VCFout[i]*ADSRout[i]*vol)/N_SNDS; //*amts[i];
        //printf("v %g\n",v);
        if(i < 10)
            v*=nn_params[i];
        mix+=v; //*nn_params[i];//finally we add the ADSR as an amplitude modulator
        
        
    }
    
    //printf("mix %g\n",mix);
    output[0]=mix*0.5;//left channel
    output[1]=mix*0.5;//right channel
    
}
#endif
