//
//  gfx.cpp
//  skeletor_visualizer
//
//  Created by Daniel Berio on 29/01/16.
//
//

#include "gfx.h"
#include "MSAInterpolator3D.h"

ofRectangle bounding_box(const std::vector<Stroke>& strokes )
{
    ofRectangle rect;
    for( int i = 0; i < strokes.size(); i++ )
    {
        if( i == 0 )
            rect = strokes[i].getBoundingBox();
        else
            rect.growToInclude(strokes[i].getBoundingBox());
    }
    return rect;
}


std::vector <float> chord_lengths( const ofPolyline & P )
{
    std::vector<float> L;
    int n = P.size();
    L.push_back(0.0);
    float s = 0.0;
    for( int i = 0; i < n-1; i++ )
    {
        ofVec2f a = P[i];
        ofVec2f b = P[i+1];
        s+= a.distance(b);
        L.push_back(s);
    }

    return L;
}

Stroke interpolate_stroke( const Stroke & P, float step )
{
    std::vector<float> X = chord_lengths(P);
    msa::Interpolator3D interp;
    interp.setInterpolation(msa::kInterpolationLinear);
    interp.setLengths(X);
    for( int i = 0; i < P.size(); i++ )
        interp.push_back(P[i]);
    int n = X.back() / step;
    std::vector<float> T = linspace<float>(0.0, 1.0, n);
    Stroke P2;
    for( int i = 0; i < T.size(); i++ )
    {
        P2.addVertex(interp.sampleAt(T[i]));
    }
    return P2;
}

Stroke merge_strokes( const std::vector<Stroke>& strokes, float interpolation_step )
{
    Stroke P;
    for( int i = 0; i < strokes.size(); i++ )
    {
        int n = P.size();
        // add zero between pen up and pen down
        if(n)
            P.addVertex(P[n-1].x, P[n-1].y, 0.0);
        Stroke next = strokes[i];
        P.addVertex(next[0].x, next[0].y, 0.0);
        // append vertices
        P.addVertices(next.getVertices());
    }
    return interpolate_stroke(P, interpolation_step);
}

enum
{
    ISOCHRONY_NONE = 0,
    ISOCHRONY_LOCAL = 1,
    ISOCHRONY_GLOBAL = 2
};

static int isochrony = ISOCHRONY_LOCAL;

Stroke spring_path( const Stroke & P, float speed, float kp, float damp_ratio, float dt, float t_mul )
{
    ofVec3f v = ofVec3f(0,0,0);
    ofVec3f a = ofVec3f(0,0,0);
    ofVec3f f = ofVec3f(0,0,0);
    
    ofVec3f p = P[0];
    
    Stroke res;
    
    float kv = damp_ratio * 2.0 * sqrt(kp);
    
    
    // local isochrony, each segment has a fixed duration independent
    float t_end = 0.0;
    switch(isochrony)
    {
        case ISOCHRONY_NONE:
            t_end = P.getPerimeter() / speed;
            break;
        case ISOCHRONY_LOCAL:
            t_end = (float)(P.size()-1) / speed;
            break;
        case ISOCHRONY_GLOBAL:
            t_end = 5.0 / speed;
            break;
    }
    
    float duration = t_end * t_mul;
    
    float t = 0.0;
    
    while( t < duration )
    {
        // equilibrium point linearly interpolated along path
        ofVec3f eq_p = P.getPointAtPercent(t/t_end);
        
        f = - v*kv + (eq_p-p)*kp;
        
        v += f*dt;
        p += v*dt;
        res.addVertex(p);
        //res.push_back(p);
        
        t+=dt;
    }
    
    return res;
}



