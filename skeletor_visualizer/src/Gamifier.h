// Actually will make this some kind of game....
#pragma once
#include "ofMain.h"
#include "ofxTextSuite.h"
#include "AudioManager.h"

class Gamifier 
{
public:
    enum
    {
        STATE_SPLASH = 0,
        STATE_GAME = 1,
        STATE_END,
        STATE_NIL = -1
    };
    
    int cur_state=STATE_GAME;
    int prev_state=STATE_NIL;
    
    // assets
    ofTrueTypeFont font_big;
    ofTrueTypeFont font_small;
    
    ofxTextBlock txt_big_centered;
    
    ofImage splash;
#ifndef TARGET_LINUX
    AudioManager audio;
#endif
    
    string taxa="FOOGOCILLUS";
    
    struct
    {
        float t = 0.0;
        float factor = 0.0;
    };
    Gamifier()
    {
    }
    
    ~Gamifier()
    {
        
    }
    
    void setup()
    {
        ofRegisterMouseEvents(this);
        ofRegisterKeyEvents(this);
        string fname="zx_spectrum-7_bold.ttf"; //"Higher.ttf";
        font_big.load(fname, 60.0*(float)ofGetScreenWidth()/1920);
        font_small.load(fname, 30*(float)ofGetScreenWidth()/1920);
        
        txt_big_centered.init(fname, 100);
        
        splash.load("splash.png");
        
#ifndef TARGET_LINUX
        audio.setup();
#endif
    }
    
    
    void update()
    {
#ifndef TARGET_LINUX
        audio.update();
#endif
    }
    
    void draw()
    {
        switch(cur_state)
        {
            case STATE_SPLASH:
            {
                draw_splash();
            }
                break;
                
            case STATE_GAME:
            {
                draw_game();
            }
                break;
                
            case STATE_END:
            {
                draw_end();
            }
                break;
                
            default:
                break;
                
        }
    }
    
    void draw_splash()
    {
        ofSetColor(255,255,255);
        splash.draw(0, 0, ofGetWidth(), ofGetHeight());
    }

    void draw_game()
    {
        ofSetColor(255,255,255);
        //font_big.drawString("BADASS GAMEFIER ", 20, 100);
        //font_small.drawString("SCORE 123213 ", 20, 200);
        
        float w = ofGetWidth() / 20;
        ofSetColor(0, 190, 255);
        float y = w+font_big.getSize()+80;
        float sp = font_small.getSize() + 7;
        font_big.drawString(taxa, 20, y); //w+font_big.getSize()+80); //ofGetHeight()-60);
        ofSetColor(255,255,255);
        y += sp + 10;
        font_small.drawString("Keys:",20,y); y += sp;
        ofSetColor(0, 190, 255);
        font_small.drawString("A - toggle animation",20,y); y += sp;
        font_small.drawString("Z - Weird mode",20,y); y += sp;
        font_small.drawString("X - Xtra limp",20,y); y += sp;
        
        /*
        stringstream s;
        s << "SCORE: " << 100;
        font_small.drawString(s.str(), 20, ofGetHeight()-10);*/
    }
    
    void draw_end()
    {
        ofSetColor(0,0,0);
        ofRect(0, 0, ofGetWidth(), ofGetHeight());
        txt_big_centered.setText("The End");
        txt_big_centered.drawCenter(ofGetWidth()/2, ofGetHeight()/2 - txt_big_centered.getHeight() / 2);
    }

    void state_changed()
    {
        switch(cur_state)
        {
            case STATE_SPLASH:
            {
            
            }
                break;
                
            case STATE_GAME:
            {
                
            }
                break;
                
            case STATE_END:
            {
                
            }
                break;
                
            default:
                break;

        }
    }
    
    void set_state(int state)
    {
        if(cur_state != state)
        {
            prev_state = cur_state;
            cur_state = state;
            state_changed();
        }
    }
    
    void gui()
    {
#ifndef TARGET_LINUX
        audio.gui();
#endif
    }
    
    // Events
    //--------------------------------------------------------------

    void keyPressed( ofKeyEventArgs& evt ){
#ifndef TARGET_LINUX
        static int fibbo[20]={A,A,B,C,E,C,C,A,D,E,D,D,C,B,E,B,B,D,A,E};
#endif
        switch(evt.key)
        {
            case ' ':
                if(cur_state==STATE_SPLASH)
//                    set_state(STATE_SPLASH);
//                else
                    set_state(STATE_GAME);
                break;
        
            case 'e':
                set_state(STATE_END);
                break;
            
            case 'n':
#ifndef TARGET_LINUX

                int n = fibbo[(rand()%20)]+OCT*(2+rand()%3);
                audio.playNote( pitch_to_frequency(n) );//*120);
#endif
            break;
        }
        
    }
    
    void keyReleased( ofKeyEventArgs& evt ){
        
    }
    
    void mouseMoved( ofMouseEventArgs& evt ){
        
    }
    
    void mouseDragged( ofMouseEventArgs& evt ){
        
    }
    
    void mousePressed( ofMouseEventArgs& evt ){
        
    }
    
    void mouseReleased( ofMouseEventArgs& evt ){
        
    }
    
    void mouseEntered( ofMouseEventArgs& evt ){
        
    }
    
    void mouseExited( ofMouseEventArgs& evt ){
        
    }
    
    void mouseScrolled( ofMouseEventArgs& evt ){
        
    }
};
