#pragma once

#include "ofMain.h"
#include "MSAPhysics2D.h"
#include "utils.h"
#include "ofxImGui.h"
#include "ofxDelaunay.h"

class Physics {
public:
    msa::physics::Particle2D_ptr mouse_particle;
    msa::physics::World2D_ptr world;

    bool do_show = true;
    bool mode_extra = false;

    vector< vector < msa::physics::Particle2D_ptr > > stroke_particles_all;    // maps particles to stroke/points
    ofColor particles_color = ofColor(255, 0, 0);
    bool do_show_particles = false;

    float drag = 0.1f;
    float radius_mult = 1.0;
    bool do_collision = false;

    int support_spring_count = 0;
    int support_particle_skip = 0;

    float random_spring_countf = 0.1;            // percentage of total particles

    bool do_spring_length_adapt = true;               //
    float spring_length_adapt_rate = 0.15;      // springs lengths adapt at this rate

    struct SpringGroup {
        string name;
        bool enabled;
        bool do_show;
        float strength2;    // sqrt of strength (i.e. actual strength is strength2 * strengths2)
        ofColor color;
        vector< msa::physics::Spring2D_ptr > springs;
    };

    const string kStrokeSprings     = "stroke";
    const string kRandomSprings     = "random";
    const string kJunctureSprings   = "juncture";
    const string kDelaunaySprings    = "delaunay";

    map <string, SpringGroup> spring_groups = {
        { kStrokeSprings,   { kStrokeSprings, true, true, 0.25, ofColor(0, 0, 0) } },
        { kRandomSprings,   { kRandomSprings, false, true, 0.2, ofColor(150, 150, 200, 100) } },
        { kJunctureSprings, { kJunctureSprings, true, true, 0.9, ofColor(0, 200, 0) } },
        { kDelaunaySprings, { kDelaunaySprings, true, true, 0.2, ofColor(150, 150, 200, 200) } },
    };

    void toggle_mode_xtra(bool force = false) {
        if(force) mode_extra + true;
        else mode_extra ^= true;

        auto&& sg = spring_groups[kRandomSprings];
        sg.strength2 = mode_extra ? 0.01 : 0.5;
        for(auto&& spring : sg.springs) {
            spring->setStrength(sg.strength2 * sg.strength2);
        }

        spring_length_adapt_rate = mode_extra ? 0.4 : 0.15;
    }

    void init(const vector<Stroke>& strokes, const vector<Juncture>& junctures) {

        // create new physics world
        world = msa::physics::World2D::create();
        world->setGravity(0)->setDrag(drag)->setNumIterations(5);
        if(do_collision) {
            world->enableCollision();
        } else {
            world->disableCollision();
        }

        // clear particles cache
        stroke_particles_all.clear();
        for(auto&& sg : spring_groups) sg.second.springs.clear();

        // iterate strokes
        for(auto&& stroke : strokes) {

            // create particles
            vector<msa::physics::Particle2D_ptr> vp;
            for(auto&& v : stroke.getVertices()) {
                vp.push_back( world->makeParticle(v)->setRadius(v.z * radius_mult) );
            }
            stroke_particles_all.push_back(vp);

            for(int i=0; i<stroke.size()-1; i++ ) {
                auto p1 = vp[i];
                auto p2 = vp[i+1];

                // create springs between particles
                float s = spring_groups[kStrokeSprings].strength2 * spring_groups[kStrokeSprings].strength2;
                auto spring = world->makeSpring(p1, p2, s, (p1->getPosition()-p2->getPosition()).length());
                spring_groups[kStrokeSprings].springs.push_back(spring);
            }
        }

        // iterate junctures
        for(auto&& juncture : junctures) {

            // iterate pairs on the juncture
            for(int i=0; i<juncture.size(); i++) {
                for(int j=i+1; j<juncture.size(); j++) {
                    ipair pid1 = juncture[i];
                    ipair pid2 = juncture[j];

                    auto p1 = stroke_particles_all[ pid1.first ][ pid1.second ];
                    auto p2 = stroke_particles_all[ pid2.first ][ pid2.second ];

                    float s = spring_groups[kJunctureSprings].strength2 * spring_groups[kJunctureSprings].strength2;
                    auto spring = world->makeSpring(p1, p2, s, 0);
                    if(spring) spring_groups[kJunctureSprings].springs.push_back(spring);
                }

            }
        }


        // do random support springs
        if(spring_groups[kRandomSprings].enabled) {
            int total_count = world->numberOfParticles() * (world->numberOfParticles() -1)* random_spring_countf;
            int cur_count = 0;
            int i=0;
            while(cur_count < total_count && i < 10000) { // don't get stuck in infinite loop!
                i++;
                auto p1 = world->getParticle(floor(ofRandom(0, total_count)));
                auto p2 = world->getParticle(floor(ofRandom(0, total_count)));

                // if not pointing to same particle
                if(p1 && p2 && p1 != p2) {
                    auto existing_spring = world->findConstraint(p1, p2, msa::physics::kConstraintTypeSpring);

                    // spring doesn't exist, create one
                    if(!existing_spring) {
                        float s = spring_groups[kRandomSprings].strength2 * spring_groups[kRandomSprings].strength2;
                        auto spring = world->makeSpring(p1, p2, s, (p1->getPosition()-p2->getPosition()).length());
                        if(spring) {
                            spring_groups[kRandomSprings].springs.push_back(spring);
                            cur_count++;
                        }

                    }

                }
            }
        }

        if(spring_groups[kDelaunaySprings].enabled) {
            static ofxDelaunay delaunay;

            delaunay.reset();
            // add points to delaunay
            for(auto&& stroke_particles : stroke_particles_all) {
                for(auto&& particle : stroke_particles) {
                    delaunay.addPoint(particle->getPosition());
                }
            }
            delaunay.triangulate();

            float s = spring_groups[kDelaunaySprings].strength2 * spring_groups[kDelaunaySprings].strength2;

            vector<ofMeshFace> faces = delaunay.triangleMesh.getUniqueFaces();
            for(const auto&face : faces) {
                vector<pair<int, int> > edges = { { 0, 1 }, { 0, 2 }, { 1, 2} };
                for(auto edge : edges) {
                    msa::physics::Particle2D_ptr p0, p1;
                    auto p0s = world->findParticles(face.getVertex(edge.first));
                    if(!p0s.empty()) p0 = p0s[0];

                    auto p1s = world->findParticles(face.getVertex(edge.second));
                    if(!p1s.empty()) p1 = p1s[0];

                    // create springs if don't exist
                    if(p0 && p1 && !world->findConstraint(p0, p1, msa::physics::kConstraintTypeSpring)) {
                        auto spring = world->makeSpring(p0, p1, s, (p0->getPosition()-p1->getPosition()).length());
                        if(spring) spring_groups[kDelaunaySprings].springs.push_back(spring);
                    }
                }

            }

        }

        toggle_mode_xtra(true);
    }


    void draw_springs(const vector<msa::physics::Spring2D_ptr>& springs, const ofColor &c) {
        ofSetColor(c);
        for(auto&& spring : springs) {
            auto a        = spring->getA();
            auto b        = spring->getB();
            ofDrawLine(a->getPosition(), b->getPosition());
        }
    }

    void adapt_spring_lengths(vector<msa::physics::Spring2D_ptr>& springs, float adapt_rate) {
        for(auto&& spring : springs) {
            auto a          = spring->getA();
            auto b          = spring->getB();
            float l         = (a->getPosition() - b->getPosition()).length();
            float l_orig    = spring->getRestLength();
            spring->setRestLength(ofLerp(l_orig, l, adapt_rate));
        }
    }


    void draw() {
        // adapt spring rest lengths
        if(do_spring_length_adapt) {
            float r = spring_length_adapt_rate * spring_length_adapt_rate;
            for(auto&& sg : spring_groups) adapt_spring_lengths(sg.second.springs, r * r);
        }


        if(do_show) {
            ofPushStyle();
            ofNoFill();

            // draw springs
            for(auto&& sg : spring_groups) if(sg.second.do_show) draw_springs(sg.second.springs, sg.second.color);

            if(do_show_particles) {
                // draw particles
                ofSetColor(particles_color);
                for(auto&& stroke_particles : stroke_particles_all) {
                    for(auto&& particle : stroke_particles) {
                        ofDrawCircle(particle->getPosition(), particle->getRadius());
                    }
                }
            }

            ofPopStyle();
        }

    }

    vector<Stroke> create_strokes_from_physics() {
        vector<Stroke> strokes;
        for(auto&& stroke_particles : stroke_particles_all) {
            Stroke stroke;
            for(auto&& p : stroke_particles) {
                ofVec3f v(p->getPosition());
                v.z = p->getRadius();
                if(radius_mult > 0) v.z /= radius_mult;
                stroke.addVertex(v);
            }
            strokes.push_back(stroke);
        }
        return strokes;
    }

    bool create_springs_gui(SpringGroup &sg) {
        bool do_init = false;
        //        ImGui::Separator();
        ImGui::CollapsingHeader((sg.name + " springs").c_str(), NULL, true, true);
        if(ImGui::Checkbox((sg.name + " enabled").c_str(), &sg.enabled)) do_init = true;
        ImGui::Checkbox((sg.name + " show").c_str(), &sg.do_show);

        if(ImGui::SliderFloat((sg.name + " strength").c_str(), &sg.strength2, 0, 1)) {
            for(auto&& spring : sg.springs) {
                spring->setStrength(sg.strength2 * sg.strength2);
            }
        }

        return do_init;
    }

    void gui(const vector<Stroke>& strokes_orig, const vector<Juncture>& junctures) {
        if(ImGui::CollapsingHeader("Physics, innit mate")) {
            if(ImGui::Button("Reset")) init(strokes_orig, junctures);
            if(ImGui::Checkbox("do_collision", &do_collision)) init(strokes_orig, junctures);
            if(ImGui::SliderFloat("radius_mult", &radius_mult, 0, 1)) init(strokes_orig, junctures);
            if(ImGui::SliderFloat("drag", &drag, 0, 1)) world->setDrag(drag);

            ImGui::Checkbox("do_show", &do_show);

            if(ImGui::Checkbox("do_spring_length_adapt", &do_spring_length_adapt));// init(strokes_orig, junctures);
            if(ImGui::SliderFloat("spring_length_adapt_rate", &spring_length_adapt_rate, 0, 1));// init(strokes_orig, junctures);

            //            if(ImGui::SliderInt("support_spring_count", &support_spring_count, 0, 10)) init(strokes_orig, junctures);
            //            if(ImGui::SliderInt("support_particle_skip", &support_particle_skip, 0, 10)) init(strokes_orig, junctures);
            if(ImGui::SliderFloat("random_spring_countf", &random_spring_countf, 0, 1)) init(strokes_orig, junctures);

            for(auto&& sg : spring_groups) if(create_springs_gui(sg.second));

        }
    }
};
