#ifndef TARGET_LINUX
#pragma once
#include "maximilian.h"
#include "threaded_player.h"
#include "TPtrList.h"
#include "utils.h"

inline float pitch_to_frequency( float pitch )
{
    return  440.0 * pow( 2.0, ((double)pitch - 69.0) / 12.0 );
}

enum
{
    A=21,
    Bf=22,
    B=23,
    C=24,
    Df=25,
    D=26,
    Ef=27,
    E=28,
    F=29,
    Gf=30,
    G=31,
    Af=32,
    OCT=12
};

struct DelayedNote
{
    DelayedNote( int note, float t )
    :
    note(note),
    t(t)
    {
        
    }
    
    int note;
    float t;
    DelayedNote * prev;
    DelayedNote * next;
};

class AudioManager : public MaxiThread
{
public:
    AudioManager();
    void setup();
    void play( double * output );
    void stop();
    
    // call me each frame
    void update();
    
    void playDelayedNotes();
    void playNote( int note );
    
    void gui();
    
    float stepPhase;
    float resonance;
    float cutoff;
    
    TPtrList<DelayedNote> delayedNotes;
    
    std::vector<int> notes;
    
    STATIC_SINGLETON(AudioManager);
};
#endif
