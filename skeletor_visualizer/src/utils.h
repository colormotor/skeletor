//
//  utils.h
//  skeletor_visualizer
//
//  Created by Daniel Berio on 29/01/16.
//
//

#pragma once

#include "ofMain.h"

#include <vector>
#include <string>
#include <map>
#include <functional>
#include <sstream>

#include <dirent.h> // wont work on Windows


typedef std::vector<std::string> string_vector;
typedef ofPolyline Stroke; // xy pos, z radius
typedef std::pair<int, int> ipair;
typedef std::vector<ipair> Juncture;

string_vector files_in_directory( const std::string& path );
std::vector<Stroke> load_strokes( const std::string & path );
std::vector<Juncture> load_stroke_junctures( const std::string & path );

template <typename T>
std::vector<T> linspace( T a, T b, int n )

{
    std::vector<T> res;
    T inc = (b-a)/(n-1);
    T v = a;
    for( int i = 0; i < n; i++ )
    {
        res.push_back(v);
        v+=inc;
    }
    res[res.size()-1]=b;
    return res;
}



#define STATIC_SINGLETON(classname) \
\
static classname*  getInstance() \
{ \
static classname instance; \
return &instance; \
}
