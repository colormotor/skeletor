#pragma once

#include "ofMain.h"
#include "utils.h"
#include "ofxImGui.h"

#ifdef TARGET_LINUX
#include "ofxMSATensorFlow.h"
#endif

#define kInputWidth     28
#define kInputHeight    28
#define kInputSize      (kInputWidth * kInputHeight)
#define kOutputSize      10

#define GO_DEEP

#ifdef GO_DEEP
#define kModelPath      "model-deep/mnist-deep.pb"
#else
#define kModelPath      "model-simple/mnist-simple.pb"
#endif

class ML {
public:
#ifdef TARGET_LINUX
    // main interface to everything tensorflow
    msa::tf::ofxMSATensorFlow    msa_tf;

    // input tensors
    tensorflow::Tensor input_tensor; // will contain flattened bytes of an image

    // pixels to store input pixels. Creating these here so they aren't reallocated every frame
    ofPixels cpix;
    ofFloatImage fimage;

    // output tensors
    // output_tensors[0] will contain 10 probabilities (one per class)
    vector<tensorflow::Tensor> output_tensors;
    //} ml;
#endif


    void init() {
#ifdef TARGET_LINUX
        // Initialize tensorflow session, return if error
        if( !msa_tf.setup() ) return;

        // Load graph (i.e. trained model) we exported from python, add to session, return if error
        if( !msa_tf.loadGraph(kModelPath) ) return;


        // initialize input tensor dimensions
        // (not sure what the best way to do this was as there isn't an 'init' method, just a constructor)
        input_tensor = tensorflow::Tensor(tensorflow::DT_FLOAT, tensorflow::TensorShape({ 1, kInputSize }));

        // MEGA UGLY HACK ALERT
        // graphs exported from python don't store values for trained variables (i.e. parameters)
        // so in python we need to add the values back to the graph as 'constants'
        // and bang them here to push values to parameters
        // more here: https://stackoverflow.com/questions/34343259/is-there-an-example-on-how-to-generate-protobuf-files-holding-trained-tensorflow/34343517
        {
            std::vector<string> names;
            int node_count = msa_tf.graph().node_size();
            ofLogNotice() << node_count << " nodes in graph";

            // iterate all nodes
            for(int i=0; i<node_count; i++) {
                auto n = msa_tf.graph().node(i);
                ofLogNotice() << i << ":" << n.name();

                // if name contains var_hack, add to vector
                if(n.name().find("_VARHACK") != std::string::npos) {
                    names.push_back(n.name());
                    ofLogNotice() << "......bang";
                }
            }

            // run the network inputting the names of the constant variables we want to run
            if( !msa_tf.run({}, names, {}, &output_tensors) ) ofLogError() << "Error running network for weights and biases variable hack";
        }
#endif
    }

    void classify() {
#ifdef TARGET_LINUX
        if(msa_tf.isReady()) {
            // get pixels from mousepainter and format correctly for input
            cpix.resize(kInputWidth, kInputHeight); // resize to input dimensions
            cpix.setImageType(OF_IMAGE_GRAYSCALE);  // convert to single channel

            // convert from unsigned char to float, this also normalizes by 1/255
            fimage.setFromPixels(cpix);

            // copy data from formatted pixels into tensor
            msa::tf::imageToTensor(fimage, input_tensor);

            // Collect inputs into a vector
            // IMPORTANT: the string must match the name of the variable/node in the graph
            vector<pair<string, tensorflow::Tensor>> inputs = { { "x_inputs", input_tensor } };

#ifdef GO_DEEP
            // set dropout probability to 1, not sure if this is the best way or if it can be disabled before saving the model
            tensorflow::Tensor keep_prob(tensorflow::DT_FLOAT, tensorflow::TensorShape());
            keep_prob.scalar<float>()() = 1.0f;
            inputs.push_back({"keep_prob", keep_prob});
#endif

            // Run the graph, pass in our inputs and desired outputs, evaluate operation and return
            // IMPORTANT: the string must match the names of the variables/nodes in the graph

            if( !msa_tf.run(inputs, { "y_outputs" }, {}, &output_tensors) )
                ofLogError() << "Error during running. Check console for details." << endl;
        }
#endif
    }


    void draw(ofTrueTypeFont &font_big, ofTrueTypeFont &font_small, ofRectangle rect) {
#ifdef TARGET_LINUX
        ofPushStyle();
        ofSetColor(255);
//        fimage.draw(0, 0, w, w);
//        ofNoFill();
//        ofDrawRectangle(0, 0, w, w);
//        ofFill();

///        stringstream str_outputs;

        // will contain outputs of network
        if(msa_tf.isReady() && !output_tensors.empty() && output_tensors[0].IsInitialized()) {

            // DRAW OUTPUT PROBABILITY BARS
            // copy from tensor to a vector using Mega Super Awesome convenience methods, because it's easy to deal with :)
            // output_tensors[0] contains the main outputs tensor
            // NB if the data was MASSIVE it would be faster to access directly from within the tensor
            vector<float> outputs;
            msa::tf::tensorToVector(output_tensors[0], outputs);

            float box_spacing = rect.getWidth() / kOutputSize;
            float box_width = box_spacing * 0.95;

            for(int i=0; i<kOutputSize; i++) {
                float p = outputs[i]; // probability of this label

                // draw probability bar
                float h = rect.getHeight() * p;
                float x = ofMap(i, 0, kOutputSize-1, rect.getLeft(), rect.getRight() - box_spacing);
                x += (box_spacing - box_width)/2;

//                ofColor c;
//                c.setHsb(i /(float)kOutputSize * 255, 255, 255);
//                ofSetColor(c);
                ofSetColor(ofLerp(50.0, 255.0, p), ofLerp(100.0, 0.0, p), ofLerp(150.0, 0.0, p), 80);
                ofDrawRectangle(x, ofGetHeight(), box_width, -h);

//                str_outputs << ofToString(outputs[i], 3) << " ";

                // draw text
//                ofDrawBitmapString(ofToString(i) + ": " + ofToString(p, 2), x, ofGetHeight() - h - 10);
                ofSetColor(255, ofLerp(50, 255, p));
                font_big.drawString(ofToString(i), x + 20, rect.getBottom() - 60);
                font_small.drawString("score: " + ofToString(p, 2), x + 20, rect.getBottom() - 30);
            }

            ofSetColor(255, 100);
            ofDrawLine(0, rect.getTop(), ofGetWidth(), rect.getTop());

        }


        ofPopStyle();
#endif
    }


    void gui() {
#ifdef TARGET_LINUX

#endif
    }
};
