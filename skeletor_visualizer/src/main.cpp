#include "ofMain.h"
#include "ofxImGui.h"
#include "utils.h"
#include "gfx.h"
#include "physics.h"
#include "ml.h"


#include "Gamifier.h"

const char* render_modes[] = {
    "Circles",
    "Quads",
    "Sprites"
};

// Dino image from http://www.dinocreta.com/wp-content/uploads/2013/11/Dinosaurs-Triceratop-River-HD-Wallpaper.jpg


class ofApp : public ofBaseApp{
public:

    ofxImGui gui;
    bool do_draw_gui = false;

    string_vector stroke_files;
    string_vector juncture_files;
    vector<ofImage> snapshots;
    
    ML ml;
    Physics physics;

    int cur_file = 0;
    int render_mode = 0;

    float rectw = 1.0;
    float recth = 0.3;
    float sprite_radius = 2.0;
    float node_radius_for_classification_image = 6;

    ofFbo fbo;

#define str_array_size(a) sizeof(a)/sizeof(const char*)

    std::vector<Stroke> strokes_orig;            // original strokes
    std::vector<Stroke> strokes_processed;       // processed strokes
    std::vector<Juncture> junctures;

    float interp_step = 0.8;

    ofImage sprite;
    ofImage sprite_sweep;
    ofImage sprite_select;
    float sprite_sweep_radius=2;
    float selector_radius=10;
    
    bool animate = false;
    float nr = 0;
    float anim_speed = 90.1;


    // Spring data
    float kp=110.0;
    float damp_ratio=0.114;
    float t_mul=1.0;
    float spring_speed=68;
    bool spring_on=false;

    Gamifier game;

    ofMatrix4x4 transform_m44;
    ofVec2f mouse_pos_transformed;
    
    bool initialized=false;

    string get_taxa()
    {
        string file = ofFilePath::getFileName(stroke_files[cur_file]);
        string taxa="";
        for( int i = 0; i < file.length(); i++ )
        {
            char c = file[i];
            if(c=='_' || c=='.')
                break;
            taxa += c;
        }
        return taxa;
    }
    void load_current()
    {
        strokes_orig = load_strokes(stroke_files[cur_file]);
        junctures = load_stroke_junctures(juncture_files[cur_file]);

        physics.init(strokes_orig, junctures);
        
        game.taxa = get_taxa();
    }

    //--------------------------------------------------------------
    void setup(){
        windowResized(ofGetWidth(), ofGetHeight());
        ofSetFrameRate(60);
        ofSetCircleResolution(64);

        gui.setup();

        stroke_files = files_in_directory(ofToDataPath("med_strokes"));
        juncture_files = files_in_directory(ofToDataPath("med_junctures"));
        load_current();

        physics.init(strokes_orig, junctures);

        ml.init();

        sprite.load(ofToDataPath("p.png"));
        sprite_sweep.load(ofToDataPath("p_sweep.png"));
        sprite_select.load(ofToDataPath("p_select.png"));
        
        game.setup(); // whohooo!
        
        create_snapshots();
    }

    void create_snapshots()
    {
        snapshots.clear();
        
        int imw=128; //ofGetHeight()-100;
        int imh=128; //ofGetHeight()-100;
        
        ofFbo fbo;
        fbo.allocate(imw, imh);
        
        //ofViewport(ofRectangle(0,0,imw,imh));
        //ofSetupScreenPerspective(imw, imh);//ofGetWidth(), ofGetHeight());//imh); //ofGetWidth(), ofGetHeight()); //, 256); //ofGetWidth(),ofGetHeight());
        for( int i = 0; i < stroke_files.size(); i++ )
        {
            fbo.begin();
            ofImage img;
            //img.allocate(imw, imh, OF_IMAGE_COLOR_ALPHA);
            ofClear(32,32,32,255);
            ofSetColor(255,255,255);
            std::vector<Stroke> strokes=load_strokes(stroke_files[i]);
            Stroke P = generate_path_for_draw(strokes);
            
            ofPushMatrix();
            draw_transform(strokes, 0, 0, imw, imh);
            draw_cont(P, 2);
            ofPopMatrix();
            
            //img.grabScreen(0, 0, imw, imh);
            //img.mirror(true, false);
            
            fbo.end();
            fbo.readToPixels(img.getPixels());
            img.update();
            
            snapshots.push_back(img);
            
            // render
        }
    }
    
    void remove_current()
    {
        if(stroke_files.size()<2)
            return;
        
        
        remove(stroke_files[cur_file].c_str());
        remove(juncture_files[cur_file].c_str());
        
        stroke_files.erase(stroke_files.begin()+cur_file);
        juncture_files.erase(juncture_files.begin()+cur_file);
        snapshots.erase(snapshots.begin()+cur_file);
        
        cur_file = 0;
        load_current();
    }
#define NDIV 20
    void select_snapshot()
    {
        float w = ofGetWidth() / NDIV;
        if(true) //ImGui::IsMouseClicked(0))
        {
            int x = ofGetMouseX() / w;
            int y = ofGetMouseY() / w;
            int i = y*NDIV+x;
            if( i < stroke_files.size())
            {
                cur_file = i;
                load_current();
            }
        }

    }
    
    void draw_snapshots()
    {
        //select_snapshot();
        
        float w = ofGetWidth() / NDIV;
        ofDisableAlphaBlending();
        
        float x = 0;
        float y = 0;
        for( int i = 0; i < snapshots.size(); i++ )
        {
            ofSetColor(255,255,255);
            ofFill();
            snapshots[i].draw(x,y,w-2,w-2);
            ofNoFill();
            if( i == cur_file )
            {
                ofSetColor(0, 190, 255);
                ofRect(x,y,w-2,w-2);
            }

            x += w;
            if( x > ofGetWidth()-w)
            {
                x=0;
                y+=w;//snapshots[i].getHeight() + 3;
            }
            
        }
        
        ofFill();
    }
    
    //--------------------------------------------------------------
    void draw_transform(const std::vector<Stroke> &strokes, int x=0, int y=0, int w=-1, int h=-1) {
        if(w==-1)
            w = ofGetWidth();
        if(h==-1)
            h = ofGetHeight();
        
        ofRectangle rect = bounding_box(strokes);
        ofRectangle dst(x, y, w, h);
        ofVec2f cenp = rect.getCenter();
        ofVec2f scr_cenp = dst.getCenter();
        float scale = std::min(dst.width / rect.width, dst.height / rect.height)*0.8;

        transform_m44.makeIdentityMatrix();
        transform_m44.glTranslate(ofVec3f(scr_cenp.x, scr_cenp.y, 0));
        transform_m44.glScale(ofVec3f(scale * 0.7, scale * 0.7, 1));
        transform_m44.glTranslate(ofVec3f(-cenp.x, -cenp.y, 0));
        ofMultMatrix(transform_m44);

        //        ofTranslate(scr_cenp.x, scr_cenp.y);
        //        ofScale(scale, scale);
        //        ofTranslate(-cenp.x, -cenp.y);
    }


    Stroke generate_path_for_draw(const std::vector<Stroke> &strokes) {
        Stroke P = merge_strokes(strokes, interp_step); //

        if( spring_on )
        {
            P = spring_path( P, spring_speed, kp, damp_ratio, 0.005, t_mul );
        }

        return P;
    }

    void draw_cont(const Stroke &P, int render_mode)
    {
        ofEnableAlphaBlending();

        ofNoFill();
        ofSetColor(255,255,255);

        int total = P.size();

        // animation
        nr = nr + ofGetLastFrameTime() * anim_speed;
        if(nr > total*2)
            nr = 0.0;

        int n_render = nr;

        if(!animate)
            n_render=total;

        switch(render_mode)
        {
        case 0:
            ofSetColor(255,255,255,128);
            for( int j = 0; j < P.size(); j++ )
            {
                ofCircle(P[j].x,
                         P[j].y,
                         P[j].z);
                if(j > n_render)
                    break;
            }
            break;
        case 1:
            // nice n slow
            for( int j = 0; j < P.size(); j++ )
            {
                ofVec2f n = P.getNormalAtIndex(j);
                ofVec3f p = P[j];
                ofPushMatrix();
                ofTranslate(p.x, p.y);
                ofRotate(ofRadToDeg(atan2(n.y, n.x)));
                ofRect(-p.z*0.5*rectw, -p.z*0.5*recth, p.z*rectw, p.z*recth);
                ofPopMatrix();

                if(j > n_render)
                    break;

            }
            break;
        case 2:
            ofSetColor(255,255,255);
            sprite.bind();
            for( int j = 0; j < P.size(); j++ )
            {
                float r = P[j].z * sprite_radius;
                sprite.draw(P[j].x - r*0.5, P[j].y-r*0.5, r, r);

                if(j > n_render)
                    break;
            }
            sprite.unbind();
            break;

        case -1:
            ofFill();
            for( int j = 0; j < P.size(); j++ )
            {
                 float r = std::min(P[j].z*node_radius_for_classification_image, node_radius_for_classification_image);
                ofCircle(P[j].x,
                         P[j].y,
                         r);
            }
            break;
        
        case -2:
            ofSetColor(255,255,255);
            sprite_sweep.bind();
            for( int j = 0; j < P.size(); j+=7 )
            {
                float r = std::min(P[j].z*sprite_sweep_radius, sprite_sweep_radius);
                sprite_sweep.draw(P[j].x - r*0.5, P[j].y-r*0.5, r, r);
            }
            sprite_sweep.unbind();

            sprite_select.bind();
            for(auto&& stroke_particles : physics.stroke_particles_all) {
                for(auto&& particle : stroke_particles) {
                    ofVec3f p = particle->getPosition(); ///strokes_orig[J[j].first][J[j].second];
                    float r = selector_radius; //P[j].z * sprite_radius;
                    sprite_select.draw(p.x - r*0.5, p.y-r*0.5, r, r);
                }
            }
            sprite_select.unbind();
                
        default:
            break;
        }


//        ofSetColor(255,0,0);
//        for( int i = 0; i < junctures.size(); i++ )
//        {
//            Juncture J=junctures[i];
//            for( int j = 0; j < J.size(); j++ )
//            {
//                ofVec3f p = strokes[J[j].first][J[j].second];
//                ofCircle(p.x, p.y, 10);
//            }
//        }

        ofFill();
    }
    

    //--------------------------------------------------------------

    void draw_gui() {
        gui.begin();

        static bool show_another_window = true;
        static bool show_test_window = true;
        printf("%d\n", str_array_size(render_modes));
        if(show_another_window)
        {
            float gui_scaler = ofGetWidth() / 1024.0f;
            ImGui::SetNextWindowSize(ImVec2(gui_scaler * 200, gui_scaler * 200), ImGuiSetCond_FirstUseEver);
            ImGui::Begin("Another Window", &show_another_window);
            ImGui::SetWindowFontScale(gui_scaler);
            if (ImGui::CollapsingHeader("Daniel's mess")) {
                ImGui::SliderFloat("Interpolation Step", &interp_step, 0.5, 3.0);
                ImGui::Combo("Render Mode", &render_mode, render_modes, str_array_size(render_modes));
                ImGui::SliderFloat("Rect w", &rectw, 0.1, 2.0);
                ImGui::SliderFloat("Rect h", &recth, 0.1, 2.0);
                ImGui::SliderFloat("Sprite Radius", &sprite_radius, 0.1, 3.0);
                ImGui::SliderFloat("Classification stroke width", &node_radius_for_classification_image, 1, 100);
                ImGui::Checkbox("Animate",&animate);
                ImGui::SliderFloat("Anim Speed", &anim_speed, 10.0, 200.0);
                ImGui::Separator();
                ImGui::Checkbox("Spring",&spring_on);
                ImGui::SliderFloat("kp", &kp, 1.0, 300.0);
                ImGui::SliderFloat("Damping", &damp_ratio, 0.0, 2.0);
                ImGui::SliderFloat("T mul", &t_mul, 0.0, 2.0);
                ImGui::SliderFloat("Spring speed", &spring_speed, 1.0, 300.0);
                ImGui::Separator();
                ImGui::SliderFloat("Line Width", &sprite_sweep_radius, 1, 100.0);
                ImGui::SliderFloat("Selector Radius", &selector_radius, 10, 300.0);
                
                if(ImGui::Button("Remove skeleton"))
                {
                    remove_current();
                }
            }

            physics.gui(strokes_orig, junctures);

            ml.gui();

            ImGui::End();

        }

        //        if(show_test_window)
        //        {
        //            ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
        //            ImGui::ShowTestWindow(&show_test_window);
        //        }

        game.gui();
        gui.end();
    }


    //--------------------------------------------------------------
    void draw(){
        ofBackground(0,0,0);
        ofEnableAlphaBlending();
        
        // update game
        game.update();

        // transform mouse
        mouse_pos_transformed =  ofVec4f(ofGetMouseX(), ofGetMouseY(), 0, 1) * transform_m44.getInverse();

        // move particle if mouse drag
        if(ofGetMousePressed())
            if(physics.mouse_particle) physics.mouse_particle->moveTo(mouse_pos_transformed);

        // update physics
        physics.world->update();

        // create strokes from physics particles
        strokes_processed = physics.create_strokes_from_physics();

        Stroke P = generate_path_for_draw(strokes_processed);

        // draw thing into fbo
        fbo.begin();
        ofPushMatrix();
        ofClear(0, 255);
        draw_transform(strokes_orig, 0, 0, fbo.getWidth(), fbo.getHeight());
        draw_cont(P, -1);
        ofPopMatrix();
        fbo.end();

        // draw fbo
        ofSetColor(255);
        float w = ofGetWidth() / NDIV;
        fbo.draw(ofGetWidth()-100, w*2, 100, 100);

        // physics
        ofPushMatrix();
        draw_transform(strokes_orig, 0, w, ofGetWidth(), ofGetHeight()-w);
        draw_cont(P, render_mode);
        physics.draw();
        draw_cont(P, -2);
        ofPopMatrix();
#ifdef TARGET_LINUX
        fbo.readToPixels(ml.cpix);
#endif
    
        ml.classify();

        ml.draw(game.font_big, game.font_small,
                ofRectangle(ofGetWidth() * 0.0, ofGetHeight()*0.8, ofGetWidth() * 1.0, ofGetHeight()*0.2));

        draw_snapshots();
        
        game.draw();


        if(do_draw_gui) draw_gui();
    }

    //--------------------------------------------------------------
    void keyPressed(int key){
        switch (key) {
        case '[':
            cur_file--;
            if(cur_file<0)
                cur_file = stroke_files.size()-1;
            load_current();
            break;

        case ']':
            cur_file = (cur_file+1)%stroke_files.size();
            load_current();
            break;

        case 'r':
            physics.init(strokes_orig, junctures);
            break;

        case 'g':
            do_draw_gui ^= true;
            break;
        
        case 'a':
            animate ^= true;
            break;
                
        case 'f':
            ofToggleFullscreen();
            break;
                
        case 'z':
            spring_on ^= true;
            break;

        case 'x':
            physics.toggle_mode_xtra();
            break;

        default:
            break;
        }
    }

    //--------------------------------------------------------------
    void keyReleased(int key){

    }

    //--------------------------------------------------------------
    void mouseMoved(int x, int y){
    }

    //--------------------------------------------------------------
    void mouseDragged(int x, int y, int button){
    }

    //--------------------------------------------------------------
    void mousePressed(int x, int y, int button){
        for(auto&& s : physics.stroke_particles_all) {
            for(auto&& p : s) {
                if(p->getPosition().distanceSquared(mouse_pos_transformed) < selector_radius*selector_radius){
                    // p->getRadius() * p->getRadius()) {
                    physics.mouse_particle = p;
                    mouseDragged(x, y, button);
                    return; // only get first one
                }
            }
        }

        select_snapshot();
    }

    //--------------------------------------------------------------
    void mouseReleased(int x, int y, int button){
        physics.mouse_particle = nullptr;
    }

    //--------------------------------------------------------------
    void mouseEntered(int x, int y){

    }

    //--------------------------------------------------------------
    void mouseExited(int x, int y){

    }

    //--------------------------------------------------------------
    void windowResized(int w, int h){
        fbo.allocate(28, 28);
    }

    //--------------------------------------------------------------
    void gotMessage(ofMessage msg){

    }

    //--------------------------------------------------------------
    void dragEvent(ofDragInfo dragInfo){

    }


    //    void draw_old()
    //    {
    //        ofEnableAlphaBlending();

    //        ofRectangle rect = bounding_box(strokes);
    //        ofRectangle dst(0, 0, ofGetWidth(), ofGetHeight());
    //        ofPushMatrix();
    //        ofVec2f cenp = rect.getCenter();
    //        ofVec2f scr_cenp = dst.getCenter();
    //        float scale = std::min(dst.width / rect.width, dst.height / rect.height)*0.8;
    //        ofTranslate(scr_cenp.x, scr_cenp.y);
    //        ofScale(scale, scale);
    //        ofTranslate(-cenp.x, -cenp.y);
    //#define NCOLORS 7
    //        static ofColor colors[NCOLORS] =
    //        {
    //            ofColor(255,0,0),
    //            ofColor(255,0,255),
    //            ofColor(0,255,0),
    //            ofColor(0,255,255),
    //            ofColor(255,255,0),
    //            ofColor(0,0,255),
    //            ofColor(255,128,0.0)
    //        };

    //        ofNoFill();
    //        ofSetColor(255,255,255);

    //        std::vector<Stroke> strokes_subd;

    //        int total = 0;
    //        for( int i = 0; i < strokes.size(); i++ )
    //        {
    //            Stroke P = interpolate_stroke(strokes[i], interp_step);
    //            strokes_subd.push_back(P);
    //            total += strokes_subd.back().size();
    //        }

    //        nr = nr + ofGetLastFrameTime() * anim_speed;
    //        if(nr > total*2)
    //            nr = 0.0;

    //        int n_render = nr;

    //        if(!animate)
    //            n_render=total;

    //        int c=0;
    //        for( int i = 0; i < strokes_subd.size(); i++ )
    //        {
    //            const ofPolyline &P = strokes_subd[i]; //interpolate_stroke(strokes[i], interp_step);

    //            ofSetColor(colors[i%NCOLORS]);
    //            P.draw();
    //            ofSetColor(255,255,255);

    //            switch(render_mode)
    //            {
    //            case 0:
    //                for( int j = 0; j < P.size(); j++ )
    //                {
    //                    ofCircle(P[j].x,
    //                             P[j].y,
    //                             P[j].z);
    //                    c++;
    //                    if(c > n_render)
    //                        break;
    //                }
    //                break;
    //            case 1:
    //                // nice n slow
    //                for( int j = 0; j < P.size(); j++ )
    //                {
    //                    ofVec2f n = P.getNormalAtIndex(j);
    //                    ofVec3f p = P[j];
    //                    ofPushMatrix();
    //                    ofTranslate(p.x, p.y);
    //                    ofRotate(ofRadToDeg(atan2(n.y, n.x)));
    //                    ofRect(-p.z*0.5*rectw, -p.z*0.5*recth, p.z*rectw, p.z*recth);
    //                    ofPopMatrix();

    //                    c++;
    //                    if(c > n_render)
    //                        break;

    //                }
    //                break;
    //            case 2:
    //                ofSetColor(255,255,255);
    //                sprite.bind();
    //                for( int j = 0; j < P.size(); j++ )
    //                {
    //                    float r = P[j].z * sprite_radius;
    //                    sprite.draw(P[j].x - r*0.5, P[j].y-r*0.5, r, r);

    //                    c++;
    //                    if(c > n_render)
    //                        break;
    //                }
    //                sprite.unbind();
    //            default:
    //                break;
    //            }

    //            if(c > n_render)
    //                break;

    //        }

    //        ofSetColor(255,0,0);
    //        for( int i = 0; i < junctures.size(); i++ )
    //        {
    //            Juncture J=junctures[i];
    //            for( int j = 0; j < J.size(); j++ )
    //            {
    //                ofVec3f p = strokes[J[j].first][J[j].second];
    //                ofCircle(p.x, p.y, 10);
    //            }

    //        }
    //        ofFill();
    //        ofPopMatrix();
    //    }
};


//========================================================================
int main( ){

    ofSetupOpenGL(1024,768, OF_FULLSCREEN);			// <-------- setup the GL context

    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp( new ofApp());

}
