//
//  gfx.h
//  skeletor_visualizer
//
//  Created by Daniel Berio on 29/01/16.
//
//

#pragma once
#include "ofMain.h"
#include "utils.h"

ofRectangle bounding_box( const std::vector<Stroke>& strokes );
//void draw_stroke( const Stroke & P );
Stroke interpolate_stroke( const Stroke & P, float step );
Stroke merge_strokes( const std::vector<Stroke>& strokes, float interpolation_step );
Stroke spring_path( const Stroke & P, float speed, float kp=70, float damp_ratio=1.0, float dt=0.001, float t_mul=1.0 );